%!TEX root = ../book.tex
\chapter{Detector Calibration}
\label{chap:calibration}

%As discussed in the previous chapters,
%The information ultimately needed when analysing data, for example,
The information needed when analysing data, for example,
the cross section measurements, is the 4-momentum vectors
of particles in interest, and possibly the knowledge about species of the particles.
For this purpose, the detector for the high energy physics is usually
designed so that it allows us to measure the momentum or energy,
and information for the particle identification, such as velocity.
However, the recorded data as they are do not tell us anything.
%The data consists of the output from ADC of the calorimeter or
%the bit streams indicating which channel of the tracker has a hit,
%and so on, but
%They are just a bunch of digits from which we don't
%know the energies or the positions, for example, at trackers
They are just a bunch of digits which are not 
energies or positions of particles
if they are not properly translated into meaningful physical variables.

This chapter describes the procedure to retrieve meaningful information
that are needed in physics analyses from raw data.
This process is called a calibration, and one of the most important
processes in the whole flow of the high energy physics experiments.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{From Raw Data to Meaningful Information}

Let's first imagine how data is generated and recorded.
As an example, suppose we take data of
electromagnetic calorimeter consisting of the sandwich
structure of lead and scintillator.
A photon hitting the calorimeter generates position-electron pair by 
photon conversion mainly in the lead plates.
The positron or electron ionises the scintillator, resulting in
scintillation light.
This scintillation light is detected by some photo-sensors such
as the photomultiplier tube~(PMT).
The electrical signal from the PMT is then converted by analog-to-digital converter~(ADC) and
recorded as the series of a bunch of digits.
Ideally the light yield of the scintillator is linear to the energy
deposit by the electron or position, and also the PMT output is
linear to the scintillation light.
With the assumption of the linearity of the light yield and the PMT
response, one can measure the energy deposit by the electron
and positron, or ultimately the incident photon energy from the
ADC counts in principle.
However, let's recall that what we have here are just ADC
counts which are solely digits.
They represent the energy, % potentially,
but do not mean energy yet
without the proper conversion to energy.
This important procedure to convert the ADC counts to energy
is called a ``detector calibration'', in short ``calibration''.

In the above example, we discussed the concept of energy calibration of the calorimeter.
This concept is very common for all detectors, whatever they measure.
Sometimes we measure the time interval of some detector signals, in which the
data is recorded by time-to-digital converter~(TDC).
In this case the calibration from TDC counts to time is needed.
Sometimes we measure the location of a charged particle hitting
the position sensitive sensor.
In this case the hit information has to be interpreted as the
position information.
This is also considered as a calibration in a sense.
In the following sections, we discuss some concrete procedures
of the detector calibrations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Detector Alignment}

The tracking device usually consists of many finely segmented channels.
Assuming we know the location of each channel, we can measure
the position of charged particles by sensing the signal from each channel.
This means that the accurate and precise knowledge for the location
of the tracking device, or more precisely the position of each channel,
including the angles hence six degrees of freedom, is crucial to
measure the hit position of the particle at the detector.
The procedure to retrieve the position of the tracking device or each
channel is called as "alignment".
Not only the tracking devices but also any other detectors consisting of
multi-channel detectors needs to be aligned as well.

The alignment procedure can be divided into two steps.
The first one is the mechanical measurement or survey of the detector
component.
In the survey, the location of the large structure of the detector is
measured, which has to be carried out usually before starting data taking
or just after installing the detector.
Since the detector element of the large structure is assembled from the
small components of sensors etc. with some precision which is specified
in each experiment, the position
of each channel is considered to be known with the precision of the
assembly (and the survey) once the survey is performed.

The second step is the alignment using charged particles.
The idea is to use such charged particles as a probe.
%by identifying the probe trajectory without using any information
%from the detector element that would be aligned.
Suppose we have tracking device composed from five layers
of silicon strip detectors, and we would like to align the
sensors of particular layer.
In this case a special tracking algorithm, which does not use the
information of the layer that will be aligned, should be prepared
and reconstruct the particle trajectory.
Then this track is extrapolated to the layer under
alignment to get the so-called residual, the difference
in position between the extrapolated probe track and the
hit on the layer.
For this reason, a higher momentum track is preferred to minimise the
extrapolation uncertainty due to the multiple scattering.
Here when we say a "hit", it is based on the hypothesis or
the priori knowledge of the location of the silicon strip sensor.
If this priori knowledge is wrong, the residual cannot be zero.
In other words, the sensor position that gives the zero residual
is likely to be the true position.
Based on this general idea, the layer under the alignment is
aligned by adjusting the location of the sensors so that
the residual distributions has a peak close to zero and
the width to be narrow.


In the actual application a slightly different approach is taken
although the basic idea is the same as we have just explained.
Because there are many layers and millions of channels in the
tracking detectors of the modern collider experiment,
it is time consuming and complicated to prepare such tracking
algorithm that doesn't use the information from the specific layers
or channels.
Instead of having such special algorithm, it is more common to
use the normal track reconstruction algorithm with looser quality
requirements to minimise the bias arising from the usage of
hit information from the layer or channel under alignment.
For each probe track, again the residual is measured.
But not only for a single layer or channels but also for all
the layers or channels in the detector under alignment, the residual is
computed.
Then the sum of residuals from many layers is calculated
for each track.
The detector is aligned so that the total residual is minimised.
This is almost equivalent to the $\chi^2$ minimisation where
%the floating parameters are the position of the sensors.
the positions of the sensors are fitted.

So far in this section, the basic concept of the alignment was given,
where we discussed the alignment of the single detector.
But the collider detector, for example, is a more complex and larger
object consisting of a several types of detectors.
Further in the actual application, the alignment is performed
in several steps.
Again using the silicon strip tracker in the ATLAS experiment as an example,
the first level is to align the whole tracker relative to the other detector system.
This means that no each layer nor single sensor is individually aligned. 
Instead, the whole support structure holding the sensors or modules
is aligned as a single object.
Then as the second level alignment, each layer is aligned,
i.e. each layer can be moved independently.
Finally as the third level, the individual module or sensor within
each layer is aligned.
In this way, the failure of the $\chi^2$ fitting due to the possible
large deviation of initial value from the actual position can be avoided.
In addition, the step-by-step approach allows to save the computing
time of the $\chi^2$ fitting.

Figure~\ref{fig:residual_pixel} shows the residual distributions
for the ATLAS silicon pixel detector, where the first alignment was
carried out by using cosmic rays and then proton-proton collision data
for more statistics.
You can see that the width becomes narrower by using the collision data,
indicating the improvement of the alignment.
Note that an old result, 
which was obtained at the very beginning of the experiment,
is intentionally present here for the illustration purpose.
Currently the width of the residual distribution is close to that
for the simulation result where all the detector position is perfectly known.
\begin{figure}
\begin{center}
\includegraphics[width=7cm]{calibration/Approved_PixECCResX.png}
\caption{
The residual distribution for the silicon pixel detector of the ATLAS experiment~\cite{ref:residual_pixel}~(ATLAS Collaboration {\copyright} 2009 CERN).
The red (black) shows the residual using proton-proton collision (cosmic ray) data.
The blue shows the prediction by simulation.
Note this plot is intentionally selected for the illustration purpose of
the effect of the alignment, not showing the current precision.
}
\label{fig:residual_pixel}
\end{center}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Momentum Scale Calibration of Magnetic Spectrometer}

We first explain the concept of measurements of charged particle momentum.
Then the calibration of the momentum scale is discussed.

\subsection{Momentum Measurement and its Resolution}

Suppose a charged particle travels in the magnetic field of $B$ (in Tesla) with the radius $R$ (in meter).
Also suppose we measure the charged particle positions by position sensitive detectors $D1$, $D2$ and $D3$,
as shown in Fig.~\ref{fig:sagitta}.
\begin{figure}[htbp]
\begin{center}
%\sidecaption
\includegraphics[scale=.3]{muon/sagitta.jpg}
%\vspace{10mm}
\caption{Sagitta measurement. See the main text for the details. 
}
\label{fig:sagitta}    
\end{center}
\end{figure}
The momentum of the charged particle $\pt$ (GeV) can be written as $p_\mathrm{T} = 0.3 \times B (\rm{Tesla}) \times R (\rm{meter})$.
Because the angle $\alpha$ in  Fig.~\ref{fig:sagitta} is geometrically represented as $\displaystyle \alpha \approx \frac{L}{R}$, 
the depth of the arc called a sagitta ($s$ in meter) of the particle trajectory can be expressed as 
\begin{equation}
s = R \left( 1-\cos{\frac{\alpha}{2}} \right) \approx R \times \frac{\alpha^2}{8} = \frac{0.3 B L^2}{8 \pt}, 
\end{equation}
where $L$ is the chord of the arc in meter. 
In case the track position at the three detectors is measured as $x_1 \pm \sigma_x$, $x_2 \pm \sigma_x$, and $x_3 \pm \sigma_x$
(with a common uncertainty of $\sigma_x$), the sagitta is $ \displaystyle s=x_2 - \frac{x_1+x_3}{2}$. 
The uncertainty of the sagitta is  $\sqrt{\frac{3}{2}} \sigma_x$.
Therefore the momentum resolution can be represented as 
\begin{equation}
\frac{\sigma_{\pt}}{\pt} = \frac{\sigma_{s}}{s} = \frac{\sqrt{\frac{3}{2}}\sigma_{x}}{s} = \frac{\sqrt{\frac{3}{2}}\sigma_{x} \cdot 8 \pt}{0.3 B L^2}.
\label{eqn:ptreso}    
\end{equation}
In the same manner, in case $s$ is measured at $N$ points ($N$ is more than about 10), the momentum resolution is represented as 
\begin{equation}
\frac{\sigma_{\pt}}{\pt} = \frac{\sigma_{x} \cdot \pt}{0.3 B L^2} \sqrt{\frac{720}{N+4}}.
\label{eqn:ptreso2}
\end{equation}
From these calculations, you can see that the momentum resolution is proportional to the momentum of charged particle and 
the uncertainty of the position measurement 
($\displaystyle \frac{\sigma_{\pt}}{\pt} \propto \sigma_x \cdot \pt$), and the inverse of the magnetic field and the square of
the length of detectors.
If you want to have better momentum resolutions, 
more detectors should be placed in a wider space where a stronger magnetic field is provided. 
This can be imagined if you draw the arc with 3 or more points in a limited space and estimate the curvature of its arc. 
For which can you estimate more precisely, an arc with a smaller radius or an arc with a larger radius?\footnote{The answer is "with a smaller radius" (under the same $B$ and $L$).} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Momentum Scale Calibration}

Going back to the calibration topics, a measurement of the trajectory of charged particle,
or more specifically the sagitta, a geometrical information of the tracking detector, and
a knowledge of the magnetic field strength are necessary to derive the momentum,
as just we have seen.	
Therefore we don't really need the momentum scale calibration for the
magnetic spectrometer in a sense, i.e. there are no conversions from a certain information
to another such as the charge-to-energy conversion in a case of the energy measurement
by a calorimeter.

But in most of the experiments, in-situ calibration or correction of the
momentum scale is performed for better accuracy and precision.
A common technique is to make use of the known mass of some
particles, for example, $K_{S}$, $J/\psi$ or $Z$.
The momentum scale of the reconstructed tracks is calibrated or
corrected so that the peak position of the invariant mass distribution
reconstructed from two tracks becomes the world average value\footnote{A physics quantity is measured by several different experiments.
Such results are combined, that is, ``averaged'', for example, by the particle data group~(PDG)~\cite{Zyla:2020zbs}. Such combined results are called world average values.}
of $K_{S}$, $J/\psi$ or $Z$.
\begin{figure}
\begin{center}
%\includegraphics[width=7cm]{calibration/Z_M_ID.png}
\includegraphics[width=7cm]{calibration/fig_Zmumu.pdf}
\caption{
The invariant mass distribution reconstructed from two oppositely
charged muons in the ATLAS experiment~\cite{ref:momentum_calibration_zmumu}~(ATLAS Collaboration {\copyright} 2011 CERN).
The background contribution is subtracted.
The data distribution is shown by either red or black dots, while the simulation
by grey histogram.
}
\label{fig:momentum_calibration_zmumu}
\end{center}
\end{figure}
Figure~\ref{fig:momentum_calibration_zmumu} shows the invariant mass
reconstructed from two oppositely charged muons.
As you can see, with this calibrated data, the peak position is consistent
with the world average value of $Z$.

The particle used as the calibration target depends on the
experiments because of the limitation of the available particles.
The data sample with high purity is always preferable to avoid
the uncertainty due to the backgrounds.
At the same time, the large data set is also preferable to reduce
the statistical uncertainty.
The experimentalist has to consider the optimal use of the
various calibration samples.

This section was devoted to describe the momentum scale calibration or correction.
But Fig.~\ref{fig:momentum_calibration_zmumu} shows the other important
point which we would like to mention.
It shows that the resolution depends on the alignment.
As can be seen in Eqs.~\ref{eqn:ptreso} and \ref{eqn:ptreso2}, momentum resolution has a linear
dependence on the precision of position measurement for a track.
Therefore better alignment leads to better resolution.
The figure shows that better alignment is used when the data was reprocessed.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Energy Calibration of Calorimeter}

The energy calibration procedure for calorimeter is classified into two steps.
The first step is to calibrate each cell or channel, and the
second is to calibrate the energy of the particle incident to
the calorimeter, equivalent to the energy of the shower after clustering.
These approaches are slightly different for the
electromagnetic and hadronic calorimeters.
Below we discuss the concept of these two-step calibration procedures for
the calorimeters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Cell-by-Cell Calibration}

In most cases, the energy information of the calorimeter is
recorded as the digital number that is converted by an ADC from the
detector output, typically the pulse height or charge created
by the sensor.
The goal of the cell-by-cell calibration is to find the relation
between the energy deposit and the ADC count for each channel,
which is a conversion factor.
A set of the factors for all the cells are called calibration constants
%and store this conversion factor as so-called
%the calibration constants.

To get this calibration constant, the most powerful and very
common technique is to use a muon as the calibration source,
because the muon in high energy physics experiment behaves
as almost a minimum ionising particle (MIP) that deposits
the constant energy per path length.
The tracking system allows to measure the path length across
the cell of the calorimeter, and hence to expect the energy deposit.
In this way, one can obtain the ADC counts for a unit energy.
Only the muons can be this kind of calibration source,
because the other charged particles evolve either electromagnetic
or hadronic shower in materials, and their energy deposits are not
constant.
On the other hand, a muon deposits its energy just by ionisation loss,
resulting in rather constant energy deposit per unit path length.
%It is difficult to evaluate the energy deposit to a specific cell
%even if you know the kinematic energy or momentum of the
%incident particle, because the shower evolution is a statistical
%process where the fluctuation is too large.
In the energy frontier collider experiments, muons decayed from
$Z$ bosons are one of the cleanest samples.
They are isolated, i.e. there are no other particles nearby, and have
high momentum.
The higher the momentum. the multiple scattering angle is smaller.
This means that the error of estimating the path length is smaller.
In addition, $J/\psi \rightarrow \mu^+ \mu^-$ events are also used
as the lower momentum calibration source.
%However, any muons can be used because of the relatively
%low fake rate of the muon identification.

The additional advantage of the usage of muons as the calibration source
is the fact that high momentum muons, which are regarded as MIPs,
are available in cosmic rays.
We can have this ideal calibration source for free in everywhere
in the world, except the underground experimental facilities
such as SuperKamiokande, although the rate of muons
is very small compared to the collider data.
%It is very fascinating that it is always available, which is in contrast
%to the collider experiment or beam test where the allocated time
%for users is very limited, and it costs a lot.

In some cases, however, the in-situ muon calibration may not be possible.
In that case, the calibration results before installing the detector
or assembling into a big piece,
%i.e. the results obtained for each single cell or module,
are used.
%As the in-situ calibration, cosmic muons are again the very
%useful calibration source.
For example, beam tests are employed, where the beam
energy is precisely known.
Or radioactive sources are also used because the energy
spectrum of the emitted particles are well known.

In addition to the calibration with particles,
a common approach is to prepare and use the artificially
generated calibration source.
For the detector whose output is lights, such as for scintillators,
light flushers like lasers can be used to emulate the signal.
For the detector whose output is electrical signals, such as
liquid argon calorimeters, electrical test pulses to the readout electronics
are often used.
By using this kind of calibration sources, the relation between
the detector output and the ADC count can be identified,
although the relation between the detector output and the energy deposit not.
Still it is useful, much better than nothing, because for example
relative gains within a detector can be monitored.
This is of particular importance for the large scale detectors
where it is not a easy task to adjust the detector response
for each individual channel.
For this reason, most of the detector system nowadays is equipped
with such a calibration device that also works as the monitoring
system of the detector performance.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Energy Cluster Calibration of Electromagnetic Shower}
\label{subsec:ecal_elemag}

In principle, once the calibration constant for each cell or channel is
obtained, one should be able to know the energy of the incident photon
or electron to the calorimeter just by summing the energy of each channel
associated to the energy cluster generated by the photon or electron.
In practice, however, the simple summing is not good enough for many reasons.
For example, the energy deposit by the electromagnetic shower is
much larger than that of muons, leading to the difficulty in the
extrapolation to higher energy.
Or there are dead materials among the active sensors consisting of
a calorimeter, where missing energy due to the dead materials
needs to be corrected.
A different clustering algorithm may lead to a different energy sum 
even for the same event.
Therefore, it is necessary to calibrate the detector in-situ with either
the electron or photon whose energy may be known without the
calorimeter information.
In this regard, the electron is a more user friendly calibration source
because other detectors rather than the calorimeter under calibration
can detect the electron and measure its momentum.
% it can be detected and measured its momentum by some
% other detectors than the calorimeter under calibration.
On the other hand, only the electromagnetic calorimeters can detect
and measure the energy of photons precisely.
This manifests the difficulty of in-situ photon calibrations in
collider experiments.

The most common and powerful technique using electrons exploits
the fact that the electron's energy deposit ($\equiv E$) at a calorimeter
should be equal to its momentum ($\equiv p$) at a tracker
because an electron deposits all the kinematic energy at the calorimeter
and we can safely ignore the electron mass in the momentum region of our interests.
%(and the electron or actually almost
%any particles we handle is highly boosted, and its mass can be neglected).
Besides, for most of the momentum range in our interests,
magnetic spectrometers consisting of charged particle tracking
devices and magnets have better momentum resolution than that of
calorimeters.
Combining above two facts, the momentum measured by the magnetic
spectrometer can be a good reference for the electromagnetic scale.
Commonly used is $E/p$ distribution where electrons or positrons make
a peak at unity if the detector is properly calibrated.

Another calibration method, which does not rely on other detectors
such as the magnetic spectrometers, makes use of the decay of
particles whose masses are precisely known.
The decays of $Z\rightarrow e^+ e^-$ and $J/\psi \rightarrow e^+ e^-$
are commonly used, where the calorimeter's response to the positron or
electron is calibrated so that the reconstructed invariant mass gets
closer to the world average value.
The width of the invariant mass distribution should be
narrower after the successful calibration.
%\begin{figure}
%\begin{center}
%\includegraphics[width=5cm,angle=270]{calibration/fig_ecal_zee.pdf}
%\caption{
%Top: The invariant mass of dielectron.
%The data is shown in blue dots, and the simulation in histogram.
%The solid (dotted) line is after (before) the correction to the simulation.
%Bottom: The ratio of data to simulation.
%}
%\label{fig:ecal_zee}
%\end{center}
%\end{figure}
%\begin{figure}
%\begin{center}
%\includegraphics[width=5cm,angle=270]{calibration/fig_ecal_jpsi.pdf}
%\caption{
%Top: The invariant mass of dielectron.
%The data is shown in blue dots, and the simulation in histogram.
%Bottom: The ratio of data to simulation.
%}
%\label{fig:ecal_jpsi}
%\end{center}
%\end{figure}
%
In the calibration using particle decays, we should be aware that
the energy of particles in the calibration source is preferred to be close
to the interesting range of your physics analysis to avoid a large extrapolation;
in the above examples, the typical electron energy is of order
of 10~GeV in $Z\rightarrow e^+ e^-$, while only a few GeV
or less in $J/\psi \rightarrow e^+ e^-$.
It means that the former should be used for relatively high \pt\ electrons and the latter for low \pt.
Finally, a decay chain with a high signal-to-noise ratio
needs to be selected to avoid a possible bias due to the background.

The calibration method using mass, for example $\pi^0 \rightarrow \gamma\gamma$, can also be used for the photon calibration in principle.
However, it is difficult to find a good decay chain which has
enough statistics and covers the wide range of photon momenta.
A lack of good calibration sources for photons is a common issue
for many experiments.
The widely used approach is to rely on the electron calibration
because the detector response by electron and photon is similar
at the first order.
They both evolve the electromagnetic shower where the only difference
is the initial depth of starting the shower.
For precision, Monte Carlo simulation is often used to correct 
small differences in the detector responses between electrons and photons.
Further when experiments become more mature or 
have more statistics, rare processes can be as the calibration source.
The example is $Z \rightarrow \mu^+ \mu^- \gamma$
where the photon is radiated off.
The statistics is much smaller than that of 
$Z \rightarrow e^+ e^-$.
The momentum range of this photon is limited because
the photon is radiated from a lepton from the $Z$ boson decay.
Still $Z \rightarrow \mu^+ \mu^- \gamma$ is used as
calibration; to be precise, it is a validation tool to
check the calorimeter response to photons.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Energy Cluster Calibration of Hadronic Shower}
\label{sec:calib_hadron}

The energy scale calibration for hadronic showers is much
more complicated than that for electromagnetic showers
for the following reasons.

First, the detector response to hadrons is different from that
to electrons or photons, because of the different shower evolutions.
Usually all the kinematic energy of particles incident to a calorimeter
is deposited in case of electromagnetic showers, meaning all the
energy can be seen by the detector.
On the other hand, some parts of the incident energy of hadrons
are often lost because the nuclear interaction length~(see Section~\ref{sec:ATLASdetector}) is
relatively longer compared to the size or depth of the detector.
Therefore, even if an electron and hadron have the same energy
and hit into the same calorimeter, the "visible" energy may
be different.
Sometimes this visible energy ratio of the electron and hadron with
the same energy is referred to as "$e/h$" ratio.
With a few exceptions, most of the hadron calorimeters have
$e/h \ne 1$, demanding a special correction in the calibration process.

Second, the fluctuation of energy deposits by hadrons is
very large, while it is almost zero for the electromagnetic
showers if the depth of the calorimeter is thick enough.
The fluctuation comes from the fact that the hadronic shower
sometimes creates $\pi^0$
that immediately decays into $\gamma \gamma$ and loses its
energy by the evolution of electromagnetic showers.
Hence in case of having $\pi^0$ in the hadronic shower, 
the visible energy gets larger, and vise versa.
Another reason of the large fluctuation is due to neutron production
in the development of hadronic shower.
In case of charged hadron production such as proton, its kinematic energy
can be detected as the energy deposit by ionisation, while neutron does not
have such energy loss mechanism and is transparent in a calorimeter.
Therefore the visible energy is influenced by the number of produced neutron.
These are the main reasons why there is rather large fluctuation in the
energy loss of the hadronic shower.
In addition, hadrons such as pion or kaon decays (semi-)leptonically,
yielding neutrinos that are not detected by the calorimeter.
The existence of neutrinos in the hadronic shower changes
the total energy deposit in the calorimeter.

By the above reasons, in order to correctly deduce the
energy of hadrons incident to the calorimeter, a
special care needs to be taken after the cell-by-cell calibration.
In the collider experiments, it is rare to have single hadron incident
to the calorimeter.
Instead, a jet (see Section~\ref{sec:jetID}) is the object handled by the calorimeter,
which is an object defined by human being.
This means that the energy of a jet depends on the
definition or actually on the clustering algorithm.
For this reason, the treatment of jet energy calibration is described after
introducing the jet reconstruction (see Section~\ref{sec:calib_jet}).
