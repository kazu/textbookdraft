%!TEX root = ../book.tex
%%%%%%%%%%%%%%%%%%%%% chapter.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% trigger
%
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%
%\motto{Use the template \emph{chapter.tex} to style the various elements of your chapter content.}
\subsection{Trigger}
\label{trig} 

The total inelastic cross-section of the proton-proton collisions
is about 100~mb at $\sqrt{s}=14$~TeV in the LHC.
When the instantaneous luminosity of the LHC accelerator is reached at
 $2 \times 10^{34}~{\rm cm}^{-2}{\rm s}^{-1}$, the rate of the 
 inelastic proton-proton interaction is expected to be about 2~GHz. 
Since the frequency of the bunch crossing in the LHC is 
designed as 40~MHz, we expect 50 proton-proton collisions 
every bunch crossing as discussed in Section~\ref{sec:accelerator}, that is called pile-up events. 
As the instantaneous luminosity goes up with the fixed rate of the bunch crossing, 
the number of the pile-up events increases more.  
On the other hand, the event rates of physics of interest, such as the production 
of the Higgs boson, are expected to be the order of 1 to 10~Hz or much less, 
depending on physics processes, as shown in Table~\ref{tbl:CrossSectionAndRate}
\begin{table}
\begin{center}
\caption{The rough cross-section and the event rate for typical processes. 
The centre-of-mass energy and the instantaneous luminosity 
in the LHC are assumed to be 13~TeV and $2 \times 10^{34}~{\rm cm}^{-2}{\rm s}^{-1}$, 
respectively.}
\label{tbl:CrossSectionAndRate}%
\begin{tabular}{l|c|c} 
\hline
Process & Cross section & Event rate \\ \hline \hline
$W$ boson production & 190~nb & 3.8~kHz \\ 
$Z$ boson production & 60~nb & 1.2~kHz \\ 
top quark pair production & 850~pb & 17~Hz\\  
Higgs boson production & 60~pb & 1.2~Hz \\
\hline
\end{tabular}
\end{center}
\end{table}
%Thus, almost all events in LHC are lots of inelastic proton interactions.
Thus, the inelastic cross-section is huge so that
even if events are produced from interesting physics processes,
% is produced,
%it is overlapped with lots of pile-up events.
they are overlapped with lots of pile-up events.

The total number of channels of the ATLAS detector is about 2 $\times$ $10^8$.
The detector sends 40~MHz $\times$ 2 $\times$ $10^8  \simeq 10^{16}$~bits 
$\simeq$ $10^{15}$~bytes (1~Peta bytes) data every second, 
in case each of the channels sends a binary digit every collision.
Although the data size per event can be reduced by a factor of about 100 using 
the noise-like data suppression and the bunch of zero-data suppression techniques, 
it is still inefficient to record all data of the proton collisions into the data storage system.
Before accumulating data of an event into the data storage, 
its event is analysed online and a decision is made
whether or not to keep the event for 
later offline study. This process is called ``trigger". 
The current ATLAS trigger and data acquisition~(DAQ) system is based on two levels 
of online event selection, called level 1 trigger (L1 trigger) and high level trigger (HLT), 
respectively, as shown in Fig.~\ref{fig:trig1}~\cite{ATLAS:2016wtr}. 
\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.18]{trigger/TriggerFlow.png}
\vspace{-20mm}
\caption{ATLAS trigger and DAQ scheme~\cite{ATLAS:2016wtr}~(ATLAS Collaboration {\copyright} 2016 CERN).}
\label{fig:trig1}       % Give a unique label
\end{center}
\end{figure}




\subsubsection{Level 1 Trigger}

The L1 trigger makes an initial selection based on huge amount of 
electronic modules 
% (printed circuit boards, multi-chip modules, 
% application-specific integrated circuits (ASICs), 
% field programable gate arrays (FPGAs)) 
(printed circuit boards equipped with application-specific 
integrated circuits (ASICs) and field programmable gate arrays (FPGAs), 
forming multi-chip modules) 
and their interconnections 
using information with reduced granularity
% reduced granularity information 
as inputs from a subset of detectors.  
There are two main L1 trigger systems in ATLAS.
The L1 calorimeter trigger~(Level-1 Calo in Fig.~\ref{fig:trig1}) is based on reduced-granularity information from 
electromagnetic and hadronic calorimeters, and searches for the high $p_{\rm T}$ 
electrons and photons, jets, and taus decaying into hadrons, as well as large 
missing and total transverse energy. 
The L1 muon trigger~(Level-1 Muon) is based on information from so-called trigger chambers; 
resistive plate chambers (RPC) in the barrel and thin gap chambers (TGC) in the endcaps 
and selects high $p_{\rm T}$ muons. 

The number of objects such as muons, electrons and photons, jets, taus 
above the set of threshold of $p_{\rm T}$ or $E_{\rm T}$ (for example,
the threshold of the muon momentum 
is set as 6~GeV, 10~GeV, 15~GeV, 20~GeV, and so on) in the fiducial region
are counted and sent to the L1 central trigger processor~(Central Trigger).
The L1 trigger provides ``region-of-interest (RoI)" information including 
position ($\eta$ and $\phi$) and $p_{\rm T}$ range of candidate objects for the input of HLT. 
In the case of the trigger based on the missing and total transverse energy, 
the information whether an event passes through the criterion of the threshold 
are sent to the L1 central trigger processor. 
The central trigger processor makes a L1 trigger decision based on the combination of 
objects required in coincidence or veto and provides the signal of the "L1 accept"~(Level-1 Accept).
The L1 trigger makes a trigger decision within about 2.5~$\mu$s and 
reduces the event rate from 40~MHz to 100~kHz.
During the process of the trigger decision, information for all detector channels has to be 
retained in ``pipeline" memories, which are placed on usually front-end electronics systems 
of the detectors~(FE in Fig.~\ref{fig:trig1}). 
The depth of the pipeline memories depends on the size of data per event, 
the frequency of the trigger latency 
\footnote{Thanks to the progress of the high-speed optical transmitter, hit information 
from all detector channels can be transmitted to the electronics modules on a counting room.}.

\subsubsection{High Level Trigger}

Only events selected by L1 trigger are read out from the front-end electronics 
systems to the readout systems~(ROS). Further trigger selections are done by the HLT.
The HLT makes more precise selection based on huge amount of the processors.
Using the RoI information the HLT selectively accesses data from readout systems.
Typically only data from a small fraction of the detector,
corresponding to RoI information provided by the L1 trigger, are
needed by the HLT.
Hence, usually only a few percent of the full event data are
required for the event processing.
The HLT makes use of information from muons, electrons, photons, jets, taus decaying into hadrons,
missing and total transverse energy, and
the charged particle tracks provided by the inner tracking system.
%Thanks to good tracking resolution of the inner tracking chambers, 
%the interaction points of proton-proton collisions can be measured, 
%jets originated from $b$-quarks and taus can be identified more precisely, 
%electrons can be discriminated from photons, and 
%the momentum resolution of the muons and electrons is improved, 
More specifically, combination of $p_{\rm T}$ or $E_{\rm T}$  of the objects above,
and topologies of events such as invariant mass and angles between the objects
are used for a decision of the HLT.
Only events accepted by the HLT are recorded in the data 
storages. The HLT reduces the event rate from 100~kHz to a few kHz.




\subsubsection{Trigger Requirements for Selecting Physics Events}

The trigger should reduce the data while keeping candidate events for further physics analyses.
%the candidate events under interest.
The target physics can be the SM process including the production of
Higgs, $W$ and $Z$ bosons, and searches for signatures beyond the SM such as
supersymmetry or other theoretical models.
The trigger needs to cover all signatures for these target physics processes using
electrons, photons, muons, jets, taus, $b$-jets, and 
missing transverse energy. 
A few thousands different trigger conditions are prepared and the list of these triggers is called a ``trigger menu''.
%The ``trigger menu" defines the list of L1 and HLT triggers. 
%A few thousands trigger lists are prepared so that all interesting 
%events are relevantly triggered.
The trigger menu is frequently updated depending on the accelerator conditions 
and physics of interest.
Practically, before starting your physics analysis, you need to design the trigger condition
to store events of your interest adequately while keeping the trigger rate of
background events low enough. 

For example, the candidates of Higgs production followed by the decay of
$H \rightarrow ZZ^* \rightarrow \mu\mu\mu\mu$  can be collected by a combination 
of the L1 muon trigger with $p_{\rm T}>15$~GeV threshold 
and the HLT with $p_{\rm T}>20$~GeV threshold. 
In this case, the trigger efficiency for muons, 
which are reconstructed to be really $p_{\rm T}>15$~GeV for L1 
and $p_{\rm T}>20$~GeV for HLT, high enough, 
while the efficiency for muons, 
which are reconstructed to be $p_{\rm T}<15$~GeV for L1 
and $p_{\rm T}<20$~GeV for HLT, is low~(Fig.~\ref{fig:mueff}).
If at least one muon out of four muons from Higgs decay 
passes through the fiducial detector volume and has $p_{\rm T}>20$~GeV, 
this kind of event can be kept for later physics analysis.
% On the other hand,
Background events from the inelastic proton-proton interaction
with a lot of low $p_{\rm T}$ particles, mostly hadrons, may
be effectively rejected by the muon trigger with the high $p_{\rm T}$ threshold. 
However, there are background events that are not removed by the trigger,
where a charged hadron is misidentified as a muon, 
a low $p_{\rm T}$ muon is mismeasured as a high $p_{\rm T}$ muon, or 
a few low $p_{\rm T}$ tracks are combinatorially reconstructed as one high
$p_{\rm T}$ muon.
As discussed in the beginning of this section,
since the cross-section of the inelastic proton-proton interaction is very high
compared to that of the interesting physics processes in most cases, 
the trigger rate can be dominated by background events even though
the misidentification and the mismeasurement of muons are rare. 
%As the instantaneous luminosity goes up, the trigger rate increases more. 
The trigger rate needs to be monitored as a function of the instantaneous luminosity shown 
in Fig.~\ref{fig:murate} and controlled by optimising, for example,
the threshold for $p_{\rm T}$ of the objects in concern.


\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.1]{trigger/MuonTrigger_effi_barrel.png}
\includegraphics[scale=.1]{trigger/MuonTrigger_effi_endcap.png}
\caption{The efficiency of the muon trigger~\cite{ATLAS:2016wtr}~(ATLAS Collaboration {\copyright} 2016 CERN). 
Top and bottom show 
the trigger efficiencies for the barrel region and  the endcap region, respectively.
The efficiency of the barrel region is lower, because in some region it is hard to 
place the muon chambers due to the interference of the toroidal magnet.
}
\label{fig:mueff}       % Give a unique label
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.1]{trigger/L1Muon_rate.png}
\includegraphics[scale=.1]{trigger/HLTMuon_rate.png}
\caption{The rate of the muon trigger~\cite{ATLAS:2016wtr}~(ATLAS Collaboration {\copyright} 2016 CERN).
}
\label{fig:murate}       % Give a unique label
\end{center}
\end{figure}
