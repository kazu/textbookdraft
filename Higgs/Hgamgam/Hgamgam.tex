%!TEX root = ../../book.tex
\subsection{$\Hgamgam$}
\label{sec:analysis_HtoGamGam}
%

The Higgs boson was discovered in the ATLAS~\cite{ATLASHiggsObs2012} and CMS~\cite{CMSHiggsObs2012} experiments in 2012.
In this discovery, $\Hgamgam$ and $\HZZ4l$ channels played the most important role because they can reconstruct the invariant mass of the Higgs boson precisely compared to other channels, for example, $\HWW2l$ even if the expected statistics for $\Hgamgam$ and $\HZZ4l$ is not high.
In the distribution of the invariant mass of the Higgs boson candidates, we can observe a clear peak of the signal on top of the background events, which is one of the most reliable evidence of a resonance particle to claim its discovery. 
In this section we explain how to search for the Higgs boson with the $\Hgamgam$ channel in the ATLAS experiment.

As mentioned before, the signal statistics is limited since the branching ratio of $\Hgamgam$ is very small, about 0.2\%, for the mass of around 125~GeV,
while thanks to a good resolution of diphoton invariant mass~$\mgamgam$, a narrow resonance was expected to be observed on a huge but smooth background as shown in Fig.~\ref{HgamgamSec:Fig:SMHgamgam_mass}~\cite{Aad:2013wqa}.
Below we'll explain how to obtain this result.

\begin{figure}[h]
\centering
\includegraphics[height=57mm]{Higgs/Hgamgam/figs/ATLAS_Hgamgam_fig_02.eps}
% https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2013-02/
\caption{Invariant mass distribution of diphotons for the combined 7~TeV and 8~TeV data in ATLAS~\cite{Aad:2013wqa}~(ATLAS Collaboration {\copyright} 2013 CERN).
The result of a fit to the data with the sum of a SM Higgs boson~(126.8~GeV) and background is superimposed.
The lower panel shows the residuals of the data with respect to the fitted background.}
\label{HgamgamSec:Fig:SMHgamgam_mass}
\end{figure}

We need two photons to reconstruct the invariant mass of diphotons, which is a final discriminant to extract signal.
Events having two photon candidates must be recorded in the offline storage to perform the analysis and diphoton triggers (35~GeV and 25~GeV for photon $\ET$) were used for the trigger selection.
Since events with jets faking photons, which are called fake photons, are not negligible, we cannot use, for example, single-photon triggers with a low $\ET$ threshold like 25~GeV\footnote{120~GeV or higher is required to use single-photon triggers, which is much higher for the photons coming from the Higgs boson of 125~GeV mass.}.
In the analysis, two photon candidates were selected with $\pT>40$~GeV and 30~GeV, which are high enough to ensure the offline selected events to achieve 100\% trigger efficiency.
This is a common technique in the physics analysis, because the estimation of trigger efficiency is not easy in general, especially for the momentum close to the turn-on of efficiency.
We can avoid to use such events near the trigger turn-on by requiring much higher $\pt$ in offline selection compared to the trigger level.
In this way the source of possible large systematic uncertainty can be removed with a cost of loosing some fraction of signal events.

There are 3 different processes in the background events: two real photons, one real photon+one fake photon and two fake photons, which are called $\gamma\gamma$, $\gamma$+jet and dijet, respectively. These background events do not make a peak but a smooth falling curve in the diphoton invariant mass~$\mgamgam$ distribution as shown in Fig.~\ref{HgamgamSec:Fig:SMHgamgam_mass}.
These compositions can be measured using photon identification variables, for example, an isolation variable. Their fractions were determined to be $\sim$74\% for $\gamma\gamma$, $\sim$22\% for $\gamma$+jet and $\sim$3\% for dijet. In addition, the Drell-Yan process~($Z^{(*)}/\gamma\rightarrow e^+e^-$, DY) remains with $\sim$1\% of the background due to hard-bremsstrahlung.

It is important to improve the resolution of the $\mgamgam$ distribution.
For this purpose we need to measure photon energy and also the angle between two photons as precisely as possible.
Since the EM calorimeter have 3 layers longitudinally in ATLAS, the direction of photons can be determined from the measurements of photon cluster positions.
The production vertex of diphotons is calculated from the direction of two photons. This method is called {\it calo-pointing}.
The position obtained with the calo-pointing is precise enough in term of the $\mgamgam$ resolution while a more precise determination is required for the association of charged tracks to jets because jets from pile-up are identified using this association information.
The production vertex position is finally obtained by using several information, for example, charged tracks not matched to any photons, charged tracks from conversions, the balance between two photons and charged tracks etc. The resolution of the $\mgamgam$ is about 3\%, and events with two unconverted photons have better resolution than those with at least one converted photon about 10\% in relative. 

Selected events are classified into several categories for two reasons; the first reason is to improve sensitivities for the search itself, which is called a global search here, and 
the second one is to measure properties of specific production processes, for example, VBF and $VH$ processes using extra leptons, jets and the missing $E_{\rm T}$.
For example, 14 categories were introduced in the 8~TeV data analysis using jets, leptons and transverse missing energy, where 2 for VBF and 3 for $VH$ and other 9 categories for the improvement of the discovery sensitivity. There was about 30\% improvement in the global search sensitivity comparing to the result without categorisation.

The event excess, which is a signature of Higgs decays, is evaluated with a local $\p0$, which is a probability of how similar an observed distribution to that with a background-only hypothesis.
If the $\p0$ value is 0.5, it indicates that the observation is consistent with the background-only hypothesis, that is, no excess.
If the $\p0$ value is smaller~(larger) than 0.5, it means there is an excess~(a deficit)\footnote{For the Higgs search in ATLAS, $\p0=0.00135$~($2.85\times10^{-7}$) corresponds to 3(5)$\sigma$, which is based on a one-sided limit.} over the background.
In addition, if a search is performed for a new narrow resonance~($\sim$4~MeV in case of SM Higgs boson) with an {\it unknown} mass in the invariant mass distribution~($\mgamgam=$[110,160]~GeV in case of SM Higgs boson),
we need to take into account so-called look-elsewhere-effect.
%before a signifincant excess~($\sim 5\sigma$ or larger) is observed, that is, in the period where an excess is getting larger.
This effect can properly treat the fact that excesses like $3\sigma$ due to the statistical fluctuation could happen even if there be no new resonance in the search region and the frequency of such fake excesses becomes high in case of narrow resonance searches\footnote{For example, we can assume that resonances with either 4~GeV and 40~GeV width could exist in the mass range of 110$-$150~GeV. In this case we may see more statistical fluctuations for 4~GeV signal than 40~GeV because the overall behaviour of the 40~GeV signal is not changed in the search range.}. The $\p0$ value after taking this effect is called a global $\p0$\footnote{In Ref.~\cite{ATLASHiggsObs2012}, the global significance of a local $5.9\sigma$ excess is estimated to be about $5.1\sigma$ in the mass range $110-600$~GeV. This result includes $\Hgamgam$, $\HZZ4l$ and $\HWW2l$.}.
This effect is negligible in case of broad resonance searches due to an intrinsic particle width, worse detector resolutions etc.
%We don't need to consider this effect once we observe a significant signal ($>5\sigma$) because the mass of such signal is already known.
With the full dataset of LHC Run~1 in ATLAS (2011-2012), the largest excess with respect to the background-only hypothesis~(based on local $\p0$) was observed~(expected) with 7.4~(4.3)$\sigma$ at 126.5~GeV as shown in Fig.~\ref{HgamgamSec:Fig:SMHgamgam_p0}~\cite{Aad:2013wqa}.


\begin{figure}[h]
\centering
\includegraphics[height=57mm]{Higgs/Hgamgam/figs/ATLAS_Hgamgam_figaux_18.eps}
% https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2013-02/
\caption{Observed local $\p0$ as a function of the Higgs boson mass $\mH$ for 7~TeV data~(blue), 8~TeV data~(red) and their combination~(black)~\cite{Aad:2013wqa}~(ATLAS Collaboration {\copyright} 2013 CERN).
The dashed curves show the expected median local $\p0$ for the SM Higgs boson hypothesis when tested at a given $\mH$.}
\label{HgamgamSec:Fig:SMHgamgam_p0}
\end{figure}
%
%\input{referenc_HiggsGamGam}
%
