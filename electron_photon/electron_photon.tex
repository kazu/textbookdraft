%!TEX root = ../book.tex
\section{Electron and Photon}
\label{sec:electron_photon}

\subsection{Interactions with Materials}
\label{sec:elec_photon_materials}
Electrons and photons lose their energy by developing a characteristic shower in the electromagnetic~(EM) calorimeter,
which is called an EM shower due to the cascade process of the bremsstrahlung~($e+\mathrm{materials}\rightarrow e\gamma$) and the $e^+e^-$ pair production~($\gamma+\mathrm{materials}\rightarrow e^+e^-$) as shown in Fig.~\ref{fig:EMShower}.
Such processes are based on the interaction of electrons and photons with materials of the absorbers, for example lead~(Pb) in the ATLAS detector and crystal~(PbWO$_4$) in the CMS detector\footnote{The ATLAS EM calorimeter is a sampling calorimeter, where the absorber is Pb and the detector is based on liquid argon and the CMS EM calorimeter is a homogeneous calorimeter, where the absorber and detector parts are made of the crystal~(PbWO$_4$).}.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\linewidth]{electron_photon/figs/EMShower.eps}
\caption{
Schematic views of a EM shower: a photon is injected into a calorimeter.
}
\label{fig:EMShower}
\end{center}
\end{figure}

A charged particle like an electron interacts with electrons in atoms~(electrons of molecules) of detector materials through the EM interaction. 
A charged particle ionises or excites atoms. This is why a charged particle loses energy in materials. This is called ionisation loss. 
This energy loss can be described by the Bethe-Bloch formula. 
In addition, a charged particle radiates photons when it is decelerated in materials, which is called bremsstrahlung. 
The ionisation loss is dominant in a low energy region (low $\beta\gamma$) but the bremsstrahlung in a high energy region (high $\beta\gamma$). 
The energy at which the ionisation loss and bremsstrahlung energy loss is equal is called a critical energy. It is about 7.3~MeV for electrons in Pb.

There are three kinds of interactions for a photon with materials: the photoelectric effect, Compton scattering and $e^+e^-$ pair production.
In the energy of $> 2m_\mathrm{electron} = 1.022$~MeV, the $e^+e^-$ pair production is dominant.
Once an $e^+e^-$ pair is created, the interaction of an electron with materials can be applicable.
This is why the detector response by a photon is similar with that by an electron at the first order as mentioned in Section~\ref{subsec:ecal_elemag}.

The development of the EM showers stops at the critical energy.
One of the key parameters to describe calorimeter performance is the radiation length $X_0$,
which represents a length with which the energy of an electron become a factor of $1/e$
by passing materials. A unit of $X_0$ is g/cm$^2$ or cm: $X_0$ for Pb is 0.56~cm, $X_0$ for PbWO$_4$ 0.89~cm.
Typical EM calorimeters have at least 20$X_0$~(ideally 25$X_0$) to stop EM showers.
Materials with smaller $X_0$ can stop EM showers with a small space.

The main materials for electrons and photons are those of calorimeter detectors.
% but
% in most real collider experiments before arriving at the calorimeter,
% there are materials
In most of actual collider experiments, however, there are materials in front of the calorimeter
where the bremsstrahlung, $e^+e^-$ pair production etc. are possible, for example, a beam pipe and inner tracking detectors.
An electron can be scattered by the Coulomb force~(called the multiple scattering) or radiate photons via the bremsstrahlung.
A photon can be converted into an $e^+e^-$ pair as shown in Fig.~\ref{fig:ATLAS_PhotonConversion}, which produces oppositely charged particles with a zero-opening angle and imbalance in momenta.
In this case, a positron and an electron are detected in the calorimeter instead of a photon.
Such a photon is called a {\it converted photon}.
The position where such a conversion happens, which is called a {\it conversion vertex}, can be obtained from these two charged tracks reconstructed in the inner tracking detector. 
When photons themselves are detected in the calorimeter without the conversion, they are often called {\it unconverted photons} to distinguish from converted photons.

\begin{figure}
\begin{center}
%\includegraphics[scale=.06]{electron_photon/figs/ATLAS_ConversionEventDisplay_ObsoleteEgammaPage.JPEG}
\includegraphics[width=0.8\linewidth]{electron_photon/figs/ATLAS_ConversionEventDisplay_ObsoleteEgammaPage.JPEG}
\caption{A photon interacts with the inner tracking detector and is converted into $e^+e^-$ (purple and green curves) in the ATLAS detector~\cite{ATLASObsoleteEgammaResults}~(ATLAS Collaboration {\copyright} 2009 CERN).
The conversion vertex is shown with a red point.}
% with $\p_\mathrm{T}$=0.79~GeV ($e^-$, purple) and $\p_\mathrm{T}$=1.75~GeV ($e^+$, green) 
%The distance from the centre of the detector is about 30~cm.}
\label{fig:ATLAS_PhotonConversion}
\end{center}
\end{figure}

\subsection{Reconstruction}
\label{sec:elec_photon_rec}
Electrons and photons are reconstructed by clustering cells of the EM calorimeter, where their energies are deposited in each cells.
% There are several different algorithms for the clustering:
Typical algorithms are:
the sliding window algorithm~\cite{Lampl:2008zz}, the topological-clustering algorithm~\cite{Aad:2016upy} etc. 
The reconstruction of electrons and photons are based on the sliding window algorithm with a sliding window seed size of $3\times 5$.
The topological-clustering algorithm is used for the isolation energy calculation in the ATLAS experiment~\cite{Aaboud:2019ynx, Aaboud:2018yqu}.
Figure~\ref{fig:clusters} shows a cluster~(yellow) by the sliding window algorithm~($5\times 7$) and several clusters~(red) by the topological-clustering algorithm.
To perform the topological-clustering, cells are categorised into, for example, three different classes: 4$\sigma$, 2$\sigma$ and 0$\sigma$ cells:
the 4$\sigma$ cells are those having energy of four or more times
larger than their expected noises~($\sigma$),
the 2~(0)$\sigma$ cells are those having energy of $2-4$~($<2$) $\times$ $\sigma$.
Then, in the step of the clustering, one of 4$\sigma$ cells is selected as a seed cell and
its neighbouring 4 or 2$\sigma$ cells in the three spatial directions are connected until there is no neighbouring 4 or 2$\sigma$ cells.
Then, all the surrounding 0$\sigma$ cells are finally connected to have clusters as electron and photon candidates.

For electrons, they can be also reconstructed through charged tracks using the inner tracking detector information (Section~\ref{sec:tracking}) since they are charged particles.
Clusters matched to a charged track are classified as electrons and those not matched to any charged tracks are as unconverted photons.
The momentum of the matched charged tracks is recalculated taking into account possible energy loss due to the bremsstrahlung in the detectors in front of the calorimeter.
A cluster matched to a track pair from a reconstructed conversion vertex or
a single track which has no hit in the innermost layer of the inner tracking detector
is classified as a converted photon. 
%Even if there is only one reconstructed track, a cluster matched to such a track is as a converted photon
%when such a track has no hit in the innermost layer of the inner tracking detector~(see the left plot of Figs.~\ref{fig:conversions}).
The energy calibration of electrons and photons are explained in Section~\ref{subsec:ecal_elemag}.


\begin{figure}
\begin{center}
\includegraphics[width=0.4\linewidth]{electron_photon/figs/fig_10.png}
\caption{Schematic view of clusters for an electron in the ATLAS experiment~\cite{Aaboud:2019ynx}~(ATLAS Collaboration {\copyright} 2019 CERN).
The $5\times 7$ cells shown by yellow colour are those obtained using the sliding window algorithm. This is used to obtain electron energy.
Other clusters~(red colour) are obtained from the topological-clustering algorithm and used to evaluate the isolation variable, which is calculated using clusters inside $\Delta R=0.4$~(a blue region).
}
%Schema of the calorimeter isolation method: the grid represents the second-layer calorimeter cells in the eta and phi directions. 
%The candidate electron is located in the centre of the purple circle representing the isolation cone. 
%All topological clusters, represented in red, for which the barycentres fall within the isolation cone are 
%included in the computation of the isolation variable. 
%The 5 x 7 cells (which cover an area of deta x dphi = 0.125 x 0.175) represented by the yellow rectangle 
%correspond to the subtracted cells in the core subtraction method. 
\label{fig:clusters}
\end{center}
\end{figure}



\subsection{Identification}
\label{sec:elec_photon_id}
Reconstructed electrons and photons are candidates of true electrons and photons, respectively.
%Other particles are also reconstructed with the same algorithms mentioned above, which are background for electrons and photons.
Other particles, which are background for electrons and photons, can be also reconstructed with the same algorithm mentioned above.
Such particles are dominated by jets, which are explained in detail later (Section~\ref{sec:jetID}).
The origin of jets is a gluon and a quark, which produce a set of particles after hadronisation.
A jet or a hadron can develop a hadronic shower in the calorimeter.
The hadronic shower, which is shown in Fig.~\ref{fig:HadShower}, has two components: hadronic and EM components. 
The hadronic component produces charged pions, charged kaons, protons, neutrons etc. through the hadronic interaction~(strong interaction)~\footnote{Muons and neutrinos are produced from the decay of charged pions via the weak interaction: $\pi^\pm \rightarrow \mu^\pm \nu$. The energy of these particles are largely undetected in the calorimeter.}.
In addition, neutral pions are also produced but they are observed as photons since the lifetime of neutral pions~($8.5\times 10^{-17}$~s) is very short and they immediately decay into two photons. This is the EM component of a hadronic shower.

\begin{figure}
\begin{center}
\includegraphics[width=0.8\linewidth]{electron_photon/figs/HadShower.eps}
\caption{
Schematic views of a hadronic shower: a hadron~($n, p, \pi^\pm$, etc.) is injected into a calorimeter.
}
\label{fig:HadShower}
\end{center}
\end{figure}

Electrons and photons can be separated from jets and hadrons using the differences between an EM shower and a hadronic shower:
lateral~($=$transverse) and longitudinal shower developments are different.
For the lateral shower shape, an EM shower is relatively narrower than a hadronic shower since the constituents of a jet~($\pi^\pm, K^\pm, p, n, \gamma$ etc.) are spread. This is the case even for a single hadron, where the hadronic shower can become wider with producing neutrons etc.
For the longitudinal shower shape, a hadronic shower is developed into the hadronic calorimeter, which is different from a EM shower,
so that energy deposited in outer layers is larger for jets and hadrons than for electrons and photons.
Variables for the identification can be defined using cells of calorimeters.
Such variables are called shower shapes variables, for example, shower widths, ratios of energy deposited in different layers of the calorimeter, etc.
Figure~\ref{fig:ATLAS_MC_electron_ID} shows four variables for electrons in the ATLAS experiment:
$w_{\eta 2}$ and $R_\eta$ represent a kind of narrowness in the lateral direction, and $R_\mathrm{had1}$ and $f_3$ for the shower development in the longitudinal direction.
Since more than 10 variables of shower shapes and tracks (if necessary) are used, so-called a multivariate analysis technique such as a combined likelihood, neural network, boosted decision tree is adopted.
% While the identification of electrons and photons is practically performed by using the shower and track variables,
% the possible reasons of misidentification are explained for electrons and photons below.
Possible reasons of misidentification for electrons and photons using the shower and track variables are given below. 

\begin{figure}
\begin{center}
\includegraphics[width=0.49\linewidth]{electron_photon/figs/figaux_08a.png}
\includegraphics[width=0.49\linewidth]{electron_photon/figs/figaux_08b.png} \\
\includegraphics[width=0.49\linewidth]{electron_photon/figs/figaux_07a.png}
\includegraphics[width=0.49\linewidth]{electron_photon/figs/figaux_07b.png}
\caption{Distributions of two shower shapes for the electron identification from the ATLAS MC simulation studies~\cite{Aaboud:2019ynx}~(ATLAS Collaboration {\copyright} 2019 CERN).
$w_{\eta 2}$ and $R_\eta$ are a shower width and a ratio of the energy in $3\times 3$ cells over the energy in $3\times 7$ cells in the second layer of the EM calorimeter, respectively.
$R_\mathrm{had1}$ is a ratio of the transverse energy~($E_\mathrm{T}$) in the first layer of the hadronic calorimeter to $E_\mathrm{T}$ of the EM cluster.
$f_3$ is a ratio of the energy in the third layer to the total energy in the EM calorimeter.
Signals are electrons from $Z$ and $J/\psi$ decay and backgrounds are
from electron candidates from multijet production, $\gamma$+jets etc.
}
\label{fig:ATLAS_MC_electron_ID}
\end{center}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics[width=0.52\linewidth]{electron_photon/figs/Fake_Jet_Elec.png}
\caption{Schematic view of a fake electron from a jet: a charged pion overlaps with photons from a neutral pion decay inside a jet.}
\label{fig:elec_fake}
\end{center}
\end{figure}

Jets can be misidentified as electrons~(called {\it fake electrons}), for example, because a charged pion overlaps with photon(s) from a neutral pion decay, a $\eta$ decay and so on inside a jet. This is illustrated in Fig.~\ref{fig:elec_fake}.
One of useful discriminating variables for this type of fake electrons is $E/p$ as shown in Fig.~\ref{fig:ATLAS_elecID_EoverP}, where $E$ is energy measured in the calorimeter and $p$ is momentum measured in the inner tracking detector.
In case of true electrons it should be close to 1 because $E$ and $p$ should originate from a same object
but in case of jets~(fake electrons) there is no clear correlation between $E$ and $p$ because different particles can contribute to $E$ or $p$.
This variable is included in the electron identification.

\begin{figure}
\begin{center}
%\sidecaption
%\includegraphics[width=0.45\linewidth]{electron_photon/figs/Eoverp_ATL-PHYS-PUB-2011-006_fig_14b.eps}
\includegraphics[width=0.65\linewidth]{electron_photon/figs/fig_13d.png}
\caption{$E/p$ distributions for electrons~(signal) and hadrons~(background) from the ATLAS MC simulation studies~\cite{Aaboud:2019ynx}~(ATLAS Collaboration {\copyright} 2019 CERN). 
$E$ is energy measured in the calorimeter and $p$ is momentum measured in the inner tracking detector.
}
\label{fig:ATLAS_elecID_EoverP}
\end{center}
\end{figure}

Not only jets but also other objects such as $\tau$-jets and converted photons are misidentified as electrons.
$\tau$-jets are misidentified when
it decays hadronically to one charged particle, so-called 
one-prong~({\it ex.} $\tau \rightarrow \pi^\pm\pi^0\nu$) with the same reason as jets, that is, the overlap between $\pi^\pm$ and $\gamma$ from $\pi^0$.
A simple $\tau$-veto algorithm is applied: electron candidates which are highly identified as $\tau$-jets in a $\tau$ identification are rejected.
For the converted photons, a cluster has a possibility to have a matched charged track when one of charged particles is not reconstructed.
To reduce such misidentification~(see Fig.~\ref{fig:conversions}), 
a track is required to associate to a primary vertex using impact parameters since tracks from a conversion vertex have large impact parameters.
In addition, a hit in the innermost layer of the inner tracking detector is required for electron candidates. 

\begin{figure}
\begin{center}
\includegraphics[width=0.48\linewidth]{electron_photon/figs/Conv1.png}
\hspace{2mm}
\includegraphics[width=0.48\linewidth]{electron_photon/figs/Conv2.png}
\caption{Schematic views of conversions at the first layer~(left) and at the beam pipe~(right).
When two tracks are reconstructed, both cases are categorised into conversions.
On the other hand, in case one of tracks is misreconstructed, only the left is taken as a conversion to reduce fake converted photons.
No hit in the first layer of the inner tracking detector is required for conversions.}
\label{fig:conversions}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.52\linewidth]{electron_photon/figs/Fake_Jet_Photon.png}
\caption{Schematic view of a fake photon from a jet: most of jet's energy is carried by a neutral pion.}
\label{fig:gamma_fake}
\end{center}
\end{figure}

Jets are misidentified as photons~(called {\it fake photons}), for example, when a neutral pion from a jet carries most of energy of the jet.
This is illustrated in  Fig.~\ref{fig:gamma_fake}.
In principle two photons should be observed inside a jet because a neutral pion decays into two photons.
To separate a single photon from a set of two photons, the finely segmented first layer is used in the ATLAS experiment as shown in Fig.~\ref{fig:ATLAS_Photon_pi0_separattion}.
A single cluster is observed for a photon~(left) but two clusters for a $\pi^0$.

\begin{figure}
\begin{center}
\includegraphics[width=0.4\linewidth]{electron_photon/figs/ATLAS_ED_20100721_photon.png}
\hspace{5mm}
\includegraphics[width=0.4\linewidth]{electron_photon/figs/ATLAS_ED_20100721_pi0.png}
\caption{Energy deposits in the three layers in the ATLAS EM calorimeter for a photon~(left) and two photons from a $\pi^0$~(right)~\cite{ATLASEMPhotons}~(ATLAS Collaboration {\copyright} 2010 CERN).
Photons are injected from the bottom to the top. The energy deposits are shown by yellow.
In the right figure, two groups of the energy deposit are observed in the first layer~(fine segmentation) of the calorimeter.}
%$\ET$=32~GeV with the isolation of 178~MeV and $\ET$=21~GeV with the isolation of 6.2~GeV.}
\label{fig:ATLAS_Photon_pi0_separattion}
\end{center}
\end{figure}

%\input{referenc_electron_photon}
