\section{Hypothesis Test}
A hypothesis test is a method to describe how well the data agree or disagree with a given hypothesis.
The hypothesis under consideration is called the null hypothesis $H_0$.
This hypothesis $H_0$ is compared with a so-called alternative or test hypothesis $H_1$ in order to quantify the compatibility of $H_0$.
In practice, $H_1$ is a hypothesis we would like to see, for example, the presence of a new particle.
In other words, $H_0$ is a hypothesis we would like to reject.

\subsection{Discovery and Exclusion}
According to the Neyman-Pearson lemma, the likelihood ratio $L(H_0)/L(H_1)$ is the optimal discriminator
for the hypothesis test $H_0$ vs. $H_1$ so that we often use the likelihood ratio as a \textit{test statistic} $t$.
However, in this section we use the number of events we select as a test statistic assuming they follow Gaussian distributions~(Eq.~(\ref{eqn:Gauss}))
to understand $p$-value etc. intuitively, where we discuss \textit{discovery} and \textit{exclusion} using the number of selected events.
Suppose that a Gaussian distribution $N_\mathrm{B}(x)$ is for background-only (the SM) and the other one $N_\mathrm{S+B}(x)$ is
for signal+background (the signal is a new physics beyond the SM).
Figure~\ref{fig:stat:alpha_beta} shows these two Gaussian distributions.
Since the $x$-axis is the number of events, the $N_\mathrm{S+B}(x)$ distribution is present in the right side of $N_\mathrm{B}(x)$.
Here, we assume that the background-only model is the the null hypothesis $H_0$ and the signal+background is for $H_1$.
For a hypothesis test, we determine a threshold $x^\mathrm{thres}$ to define a \textit{significance level} $\alpha$.
In this case, the $\alpha$ is defined as 
\begin{equation}
\alpha = \int^\infty_{x^\mathrm{thres}} N_\mathrm{H_0:B}(x) dx, \nonumber
\end{equation}
which is shown in Fig.~\ref{fig:stat:alpha_beta}.
Then, if $H_0$ is false and $H_1$ is true, the probability to reject $H_0$ correctly is called a \textit{power} $1-\beta$ where the $\beta$ is defined as
\begin{equation}
\beta = \int^{x^\mathrm{thres}}_{-\infty} N_\mathrm{H_1:S+B}(x) dx, \nonumber
\end{equation}
which is also shown in Fig.~\ref{fig:stat:alpha_beta}.
These $\alpha$ and $\beta$ also correspond to \textit{type I error}~(\textit{false positive}) and \textit{type II error}~(\textit{false negative}), respectively.
The former is the probability to reject $H_0$ wrongly and the latter is one to reject $H_1$ wrongly.
Then, we assume that we obtain the number of events $x^\mathrm{obs}$ from data.
We define a $p$-value $p$, which is a probability to show the compatibility with $H_0$:
\begin{equation}
p = \int^\infty_{x^\mathrm{obs}} N_\mathrm{H_0:B}(x) dx. \nonumber
\end{equation}
When the value of $p$ is smaller than $\alpha$, we can say that $H_0$ is rejected by the \textit{significance level} of $\alpha$.

\begin{figure}[htbp]
\begin{center}
%\sidecaption
\includegraphics[width=0.48\linewidth]{statistics/hypo/alpha_beta.png}
\caption{Probability density distributions for $H_0$~(left) and $H_1$~(right) hypotheses. 
Given a threshold on the number of events~(``thres''), the regions for the \textit{significance level} of $\alpha$ and the \textit{power} $1-\beta$ are shown.}
\label{fig:stat:alpha_beta}
\end{center}
\end{figure}

Figure~\ref{fig:stat:discovery_exclusion}(a) shows an example of the discovery, where $H_0$ is the background-only and $H_1$ is the signal+background.
We use the $p$-value $p_0$ under $H_0$ to claim the discovery of a new particle\footnote{The suffix of 0 is often used for the background-only.}.
Conventionally, if $p_0$ is smaller than $2.87\times 10^{-7}$, what we observe is very rare under $H_0$ so that we consider that $H_0$ is rejected.
The $p$-value can be transformed to $z$-value, which is defined using a standard Gaussian distribution as
\begin{equation}
p = \int^\infty_{z} \frac{1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}} dx. \nonumber
\end{equation}
For $p=2.87\times 10^{-7}$, $z$-value corresponds to $5\sigma$, which is shown in Table~\ref{tbl:sig-err}.
When we investigate new physics models using MC samples~(MC studies),
the observed $x^\mathrm{obs}$ is replaced with the median of the $N_\mathrm{H_1:S+B}(x)$ distribution.
To claim so-called ``evidence'' instead of ``discovery'', we conventionally use $p=1.35\times 10^{-3}$~($3\sigma$).

\begin{figure}[htbp]
\begin{center}
\subfigure[]{\includegraphics[width=0.49\linewidth]{statistics/hypo/discovery_num.png}}
\subfigure[]{\includegraphics[width=0.49\linewidth]{statistics/hypo/exclusion_num.png}}
\caption{(a) $p_0$ for discovery and (b) $1-p$ for exclusion. They are evaluated for the number of observed events~(``obs'').
In case of MC studies, the number of observed events is replaced with the median of signal+background and background-only for (a) and (b), respectively.}
\label{fig:stat:discovery_exclusion}
\end{center}
\end{figure}

Figure~\ref{fig:stat:discovery_exclusion}(b) shows an example of the exclusion of a model, where $H_0$ is the signal+background and $H_1$ is the background-only.
When the value of $(1-p)$ under $H_0$ is smaller than 0.05, $H_0$ is rejected.
We call it ``95\% exclusion.''\footnote{In some experimental results, 90\% is also used instead of 95\%. For 90\%, the value of $(1-p)$ is set to be 0.1.}
In case of MC studies, the $x^\mathrm{obs}$ is replaced with the median of the $N_\mathrm{H_1:B}(x)$ distribution.
The $(1-p)$ is denoted as $\mathrm{CL}_{s+b}$ in high-energy experiments
since the $(1-p)$ value corresponds to compatibility with the signal+background hypothesis.
Furthermore, in the LHC experiments, a $\mathrm{CL}_{s}$ based exclusion is often used instead of $\mathrm{CL}_{s+b}$.
$\mathrm{CL}_{s}$ is defined as
\begin{equation}
\mathrm{CL}_{s} = \frac{\mathrm{CL}_{s+b}}{\mathrm{CL}_{b}} = \int^{x^\mathrm{obs}}_{\infty} N_\mathrm{H_0:S+B}(x) dx / \int^{x^\mathrm{obs}}_{\infty} N_\mathrm{H_1:B}(x) dx. \nonumber
\end{equation}
In case of MC studies, the denominator is 0.5 because $x^\mathrm{obs}$ is the median of the $N_\mathrm{H_1:B}(x)$ distribution;
$\mathrm{CL}_{s}$ is $2\mathrm{CL}_{s+b}$ so that the 95\% exclusion using $\mathrm{CL}_{s}$ corresponds to the $(1-p)$ of 0.025 for $\mathrm{CL}_{s+b}$.
The $\mathrm{CL}_{s}$ is not a probability but in order to avoid incorrect exclusions, which could be possible when the expected signal is small,
the LHC experiments often use it to claim exclusions.

\subsection{Profile Likelihood Fit}
Suppose we count the number of observed events $n$ after applying our event selection.
This parameter of $n$ follows a Poisson distribution with an expectation value of $\mu s+b$,
where $s$ is the expected value from a signal model,
$b$ is the expected value from background processes.
The likelihood function can be defined as
\begin{equation}
L(\mu,s,b) = \frac{(\mu s+b)^n}{n!}e^{-(\mu s+b)}. \nonumber
\end{equation}
The parameter of $\mu$ is a scale factor of the signal and is called a \textit{signal strength}.
Given $s$ and $b$, we can extract a $\mu$ value from a fit to data, which gives the value of $n$, using the maximum likelihood technique explained in Section~\ref{sec:likelihood}.
If the data would follow the assumed signal model, $\mu$ be 1
but if the data would follow the SM, that is, background-only, $\mu$ be 0.

We modify this likelihood function by adding more terms.
Since the $s$ corresponds to $L\sigma_\mathrm{phys}A\epsilon$ of Eq.~(\ref{eqn:xsec}),
we can consider systematic uncertainties from these parameters~($L$, $\sigma_\mathrm{phys}$, $A$, $\epsilon$).
For example, the uncertainty on the integrated luminosity, 
the scale uncertainties (factorisation and renormalisation scales) on the $\sigma_\mathrm{phys}$,
the uncertainties from jet energy scale etc. on the $\epsilon$ and so on can be systematic uncertainties for $\mu$.
We often use Gaussian terms\footnote{A log-normal term etc. can be used instead of a Gaussian term, for example, if we require a positive definition.}
to constrain the signal term and also other terms in $b$.
Furthermore, in most of data analysis,
we estimate the background $b$ in the signal region, which is defined by our (signal) event selection,
by using a so-called control or sideband regions in data with helps of MC samples.
In this case, the $b$ in the signal region can be described with $\eta_\mathrm{tf}(\alpha_\mathrm{tf})b$,
where $b$ is the number of events in the control region, $\eta_\mathrm{tf}$ is a scale~(transfer) factor
from the control region to the signal region. The $\eta_\mathrm{tf}$ is obtained from both data and
MC samples so that some additional constraints~($\alpha_\mathrm{tf}$) are possible.
In the end, one of examples of a final likelihood function can be written as
\begin{equation}
\begin{split}
L(\mu,\bm{\theta})
&=      Pois(n|\mu\eta_s(\alpha_s)s+\eta_\mathrm{tf}(\alpha_\mathrm{tf})b)\cdot N(\alpha_s|0,1) \cdot \\
&\quad  Pois(m|\eta_b(\alpha_b)b)\cdot N(\alpha_b|0,1) \cdot \\
&\quad  N(\alpha_\mathrm{tf}|0,1), \nonumber
\end{split}
\end{equation}
where $\bm{\theta} = (b,\alpha_s,\alpha_b,\alpha_\mathrm{tf})$, $\eta_i(\alpha)=\mu_i+\sigma_i\alpha$, $Pois(n|\mu)=\mu^n e^{-\mu}/n!$, and
$N(x|\mu,\sigma)=1/(\sqrt{2\pi}\sigma)\cdot \exp(-(x-\mu)^2/(2\sigma^2))$.
The $m$ is the number of observed events in the control region.
The $\bm{\theta}$ is called a set of \textit{nuisance parameters}, which are determined by the likelihood fit with the $\mu$.
The $\eta_i$ is a scale parameter for signal, background and so on.
The $\alpha_i$ is a parameter to adjust the $\eta_i$ through a Gaussian constraint.
Parameters $\mu_i$ and $\sigma_i$ for $\eta_i$ describe centre values and their uncertainties and are evaluated from other studies before the likelihood fit.
If the $\alpha_i$ value is 0, the value of $\eta_i$ becomes $\mu_i$. If not, the value of $\eta_i$ is varied from its centre value.
Practically, $\mu_i$ is close to 1.
Then, the effect on the signal strength $\mu$ from each constraint is determined in the maximum likelihood~(ML) fit.
It means that the systematic uncertainties on the $\mu$ from each constraint term are simultaneously determined with the $\mu$ value itself.
We call this procedure a ``profiled'' fit.
When the pre-studies on $\mu_i$ and $\sigma_i$ are proper, the values of $\alpha_i$ are expected to be close to $0\pm 1$.
For example, if the error of $\alpha_i$ is smaller than 1 (say 0.3 or 0.4), it means that the value of $\sigma_i$ given from the pre-studies
is tightly constrained from data used in the ML fit, for example, data of control regions.
If this is not expected, some additional studies might be required to understand such small values.

\subsection{Profile Likelihood Ratio}
We introduce the following likelihood ratio as a test statistics $t_\mu$:
\begin{equation}
t_\mu = -2\ln\lambda (\mu), \nonumber
\end{equation}
\begin{equation}
\lambda(\mu) = \frac{L(\mu,\hat{\hat{\bm{\theta}}})}{L(\hat{\mu},\hat{\bm{\theta}})}, \nonumber
\end{equation}
where the denominator of $\lambda (\mu)$ is maximised for both $\mu$ and $\bm{\theta}$ (an unconditional ML fit) but
the numerator is maximised for $\bm{\theta}$ with respect to a specified $\mu$ value (a conditional ML fit).
Since the denominator corresponds to the best fit to data, the value of $\lambda (\mu)$ is $0 < \lambda (\mu) \le 1$
so that the value of $t_\mu$ is 0 or positive.
When the numerator with a specified $\mu$ value follow the data, the $t_\mu$ can be small but 
if not, the $t_\mu$ becomes large.
The $p$-value is defined as
\begin{equation}
p_\mu = \int^\infty_{t_{\mu,\mathrm{obs}}} f(t_\mu|\mu') dt_\mu, \nonumber
\end{equation}
where $t_{\mu,\mathrm{obs}}$ is the value of the observed $t_\mu$ and
$f(t_\mu|\mu')$ is the probability density distribution of $t_\mu$ under the assumption of the signal strength $\mu'$.
The advantage of the use of this test statistics is that
the distribution of $t_\mu$ follows a $\chi^2$ distribution of one degree-of-freedom: $f(t_\mu|\mu) \sim \chi^2_{\mathrm{dof}=1}(t_\mu)$,
so that we can evaluate $p$-value without toy Monte Carlo\footnote{For $f(t_\mu|\mu')$, we can use a noncentral $\chi^2$ distribution of one degree-of-freedom.}.
We explain the overall idea of the discovery and exclusion using this test statistics below
but the technical detail of the hypothesis test using this test statistics can be found in Ref.~\cite{Cowan:2010js}.

In high-energy experiments, we search for a new signal particle
by checking an excess over the expected events of a background-only assumption.
The signal existence corresponds to $\mu > 0$.
For this case, an alternative test statistic $\tilde{t}_\mu=-2\ln\tilde{\lambda}(\mu)$ is introduced,
\begin{equation}
\tilde{\lambda}(\mu) = 
%\left\{ 
%\begin{gathered}
\begin{cases}
\frac{L(\mu,\hat{\hat{\bm{\theta}}}(\mu))}{L(\hat{\mu},\hat{\bm{\theta}}(\hat{\mu}))} \quad &(\hat{\mu}\ge 0) \\
\frac{L(\mu,\hat{\hat{\bm{\theta}}}(\mu))}{L(0,\hat{\bm{\theta}}(0))} \quad &(\hat{\mu}< 0),
\end{cases}
%\end{gathered}
%\right.
\label{eqn:tilde_lambda}
\end{equation}
where the best-fit $\mu$ value with a deficit~($\hat{\mu}< 0$) is replaced with $\mu=0$.

\subsubsection{Discovery}
%For discovery, 
We test $\mu=0$, that is, we reject the null hypothesis $H_0$ of $\mu=0$~(background-only).
We use a special notation $q_0=\tilde{t}_0$ for this case. 
From Eq.~(\ref{eqn:tilde_lambda}), we use
\begin{equation}
q_0 = 
%\left\{
%\begin{gathered}
\begin{cases}
-2\ln\lambda(0) = -2\ln\frac{L(0,\hat{\hat{\bm{\theta}}}(0))}{L(\hat{\mu},\hat{\bm{\theta}}(\hat{\mu}))} \quad &(\hat{\mu}\ge 0) \\
0 \quad &(\hat{\mu}< 0). \nonumber
\end{cases}
%\end{gathered}
%\right.
%\label{eqn:q0}
\end{equation}
We get a single value of $q_0$ from data, $q^\mathrm{obs}_0$, and evaluate $p$-value $p_0$ using
\begin{equation}
p_0 = \int^\infty_{q^\mathrm{obs}_0} f(q_0|0)dq_0, \nonumber
\end{equation}
where the $f(q_0|0)$ is a distribution of $q_0$ made under the assumption of $\mu=0$.
Figure~\ref{fig:stat:q0_q1}(a) shows distributions of $q_0$ for the assumption of $\mu=0$ and 1: $f(q_0|0)$ and $f(q_0|1)$.
Once we obtain a distribution $f(q_0|0)$, we can evaluate $p_0$ using $q^\mathrm{obs}_0$.
Then, when the $p_0$ value is smaller than $2.87\times 10^{-7}$, we can claim discovery\footnote{In some special cases, 
we may take into account so-called \textit{look-elsewhere effect}. In this case, the standard $p_0$ value is called a local $p_0$ and 
the $p_0$ after the look-elsewhere effect is called a global $p_0$.
In case of the Higgs search in the LHC experiments~(see Section~\ref{sec:analysis_HtoGamGam}),
this effect was taken into account because the Higgs mass is unknown and
we searched for a Higgs signal in a certain mass range which is much wider than resolutions~(Higgs natural width~$\otimes$~detector),
where we expect some statistical fluctuations even if we would have a true Higgs.
The practical method is discussed in Ref.~\cite{Gross:2010qma}.
}.

We can approximate the $f(q_0|0)$ distribution as follows,
\begin{equation}
f(q_0|0) = \frac{1}{2}\delta(q_0)+\frac{1}{2}\frac{1}{\sqrt{2\pi}}\frac{1}{\sqrt{q_0}}e^{-q_0/2}. \nonumber
\end{equation}
By using this equation, a $z$-value $Z$ can be obtained as
\begin{equation}
Z = \sqrt{q^\mathrm{obs}_0}. \nonumber
\end{equation}
The $5\sigma$ discovery corresponds to $q^\mathrm{obs}_0=25$.
In Fig.~\ref{fig:stat:q0_q1}(a), the value of $q^\mathrm{obs}_0$ is 23, which is just an example, so that we cannot say the ``discovery.''
In case of MC studies, as shown in Fig.~\ref{fig:stat:q0_q1}(a), we can use the median of the $f(q_0|1)$ distribution as $q^\mathrm{obs}_0$.
In this case, $q^\mathrm{med}[f(q_0|1)]$ is smaller than 25 so that we cannot claim the ``discovery''\footnote{The value of $q^\mathrm{med}[f(q_0|1)]$ is larger than 9 so that we may say the ``evidence.'' For the ``discovery'', we may need $s=\sim 36$ in this example.} by a physics model of $s=20$.

\begin{figure}[htbp]
\begin{center}
\subfigure[]{\includegraphics[width=0.49\linewidth]{statistics/hypo/q0.eps}}
\subfigure[]{\includegraphics[width=0.49\linewidth]{statistics/hypo/q1.eps}}
\caption{(a) $f(q_0|0)$ with $f(q_0|1)$ for discovery (b) $f(q_1|1)$ with $f(q_1|0)$ for exclusion~(upper limit). 
$f(*|0)$ and $f(*|1)$ show distributions for background-only and signal+background events, respectively.
A likelihood function of $L(\mu,\theta=b) = \frac{(\mu s+b)^n}{n!}e^{-(\mu s+b)} \cdot \frac{b^m}{m!}e^{-b}$ is used, 
where variables of $n$ and $m$ are the number of events observed in the signal and control regions and $s=20$ and $b=10$ are used in this example;
$b$ in the signal region is estimated from the value of $b$ in the control region.
Dashed curves are central~(for blue) and noncentral~(for red) $\chi^2$ distributions of one degree-of-freedom.
For the noncentral cases, so-called Asimov data, which is defined as data produced with the expectation values of inputs~($s$,$b$ and $\mu$),
is used to evaluate a width required in an approximate formula of $f(q_\mu|\mu')$~\cite{Cowan:2010js}.
}
\label{fig:stat:q0_q1}
\end{center}
\end{figure}

\subsubsection{Exclusion or Upper Limit}
We test $\mu(\neq 0)$, that is, we reject the null hypothesis $H_0$ of signal+background model.
When a specified $\mu$ value is equal to or smaller than the $\hat{\mu}$ of the unconditional ML fit, we consider that $q_\mu$ is 0.
It means that the exclusion of models is performed for only $\mu$ values which are larger than the observed best-fit $\mu$.
We define $q_\mu$ as
\begin{equation}
q_\mu = 
%\left\{
%\begin{gathered}
\begin{cases}
-2\ln\lambda(\mu) \quad &(\hat{\mu}\le \mu) \\
0 \quad &(\hat{\mu}> \mu). \nonumber
\end{cases}
%\end{gathered}
%\right.
%\label{eqn:qmu}
\end{equation}
We evaluate $p$-value $p_\mu$ using
\begin{equation}
p_\mu = \int^\infty_{q^\mathrm{obs}_\mu} f(q_\mu|\mu')dq_\mu, \nonumber
\end{equation}
where the $f(q_\mu|\mu')$ is a distribution of $q_\mu$ made under the assumption of $\mu'$.
Figure~\ref{fig:stat:q0_q1}(b) shows distributions of $q_{\mu=1}$~(simply $q_1$) for the assumption of $\mu=0$ and 1.
%Once we obtain a distribution $f(q_\mu|\mu')$, we can evaluate $p_\mu$ using $q^\mathrm{obs}_\mu$.
When the $p_\mu$ value is smaller than 0.05, we can claim 95\% $\mathrm{CL}_{s+b}$ exclusion.
This corresponds to $q^\mathrm{obs}_\mu>2.69(=1.64^2)$.
In Fig.~\ref{fig:stat:q0_q1}(b), the observed $q_1$ (23 as an example) can claim the ``95\% $\mathrm{CL}_{s+b}$ exclusion'',
where a model of $s=20$ cannot be explained by the background-only.
Practically, we need a scan of $\mu$ values to find a $\mu$ value having $p_\mu=0.05$. This corresponds to $\mu\sim 0.4$ in case of Fig.~\ref{fig:stat:q0_q1}(b).
For 95\% $\mathrm{CL}_{s}$ exclusion, we use a distribution $f(q_\mu|0)$ to evaluate $\mathrm{CL}_{b}=\int^\infty_{q^\mathrm{obs}_\mu} f(q_\mu|0)dq_\mu$.
In case of MC studies, as shown in Fig.~\ref{fig:stat:q0_q1}(b), we can use the median of the $f(q_\mu|0)$ distribution as $q^\mathrm{obs}_\mu$ and $\mathrm{CL}_{b}=0.5$.
For 95\% $\mathrm{CL}_{s}$ exclusion of MC studies, we can use $q^\mathrm{obs}_\mu>3.84(=1.96^2)$.

For the case where we consider models with $\mu\ge 0$, we can define and use an alternative test statistic $\tilde{q}_\mu$:
\begin{equation}
\tilde{q}_\mu = 
%\left\{
%\begin{gathered}
\begin{cases}
-2\ln\frac{L(\mu,\hat{\hat{\bm{\theta}}}(\mu))}{L(0,\hat{\bm{\theta}}(0))} \quad &(\hat{\mu}< 0) \\
-2\ln\frac{L(\mu,\hat{\hat{\bm{\theta}}}(\mu))}{L(\hat{\mu},\hat{\bm{\theta}}(\hat{\mu}))} \quad &(\mu\ge\hat{\mu}\ge 0) \\
0 \quad &(\hat{\mu}> \mu). \nonumber
\end{cases}
%\end{gathered}
%\right.
%\label{eqn:tilde_qmu}
\end{equation}
The procedure similar to the case of $q_\mu$ can be applied~\cite{Cowan:2010js}.
