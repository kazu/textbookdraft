%!TEX root = ../book.tex
%%%%%%%%%%%%%%%%%%%%% chapter.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% muon
%
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%
%\motto{Use the template \emph{chapter.tex} to style the various elements of your chapter content.}
\section{Muon}
\label{muon} % Always give a unique label
% use \chaptermark{}
% to alter or adjust the chapter heading in the running head

Muons can be produced via decays of Higgs bosons, $W$/$Z$ bosons, quarks and new particles such as SUSY. 
Therefore the reconstruction and identification of the muons with good quality in the wide range of momentum and solid angle  
are key to many of the most important physics in the energy frontier experiment. 

The muon belongs to the second generation lepton. The characteristics is similar to the electron except for the mass, 
with an electronic charge of $-e$, 
a spin of 1/2, a mass~($m_\mu$) of $105.6583715 \pm 0.0000035~{\rm MeV}$. 
Muons hardly make either electromagnetic or hadronic shower in our energy regime, 
but decay into an electron, an electron anti-neutrino, and a muon neutrino via the weak interaction.  
Therefore the mean lifetime of the muon ($\tau_\mu$), $2.1969811\pm0.0000022~\mu{\rm s}$, is relatively long.  
Muons with the momentum ($p_\mu$) of 1.0~GeV can pass through around 70~m at a period of the mean lifetime 
in the laboratory frame; 
\begin{equation}
c \tau_\mu \beta \gamma = c \tau_\mu \frac{p_\mu}{m_\mu} \approx 3 \times 10^{8}~({\rm m/s}) \times 2.2 \times 10^{-6}~({\rm s}) \times \frac{1.0~({\rm GeV})}{0.105~({\rm GeV})} \approx 70~({\rm m})
\end{equation}
where the $c$ is a velocity of light, $\displaystyle \beta=\frac{v}{c}$ is a ratio of the velocity of the muon to the light velocity, 
and $\displaystyle \gamma=\frac{1}{\sqrt{1-\beta^2}}$ is a Lorentz boost factor. 

Considering these unique characteristics of the muon, in the collider experiments, 
muons can be detected by charged particle detectors located at both of inside and outside of calorimeters. 
%Even muons can be identified with the energy deposit in the
%calorimeters.
In this section, the muon identification and reconstruction in the collider experiments are described using
the ATLAS detector as an example.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Muon Momentum Measurement}
\label{Basics_muon_momentum_measurement}
Muon reconstruction and identification in ATLAS relies on inner tracking detector, described in
Section~\ref{sec:ATLASdetector}, and muon spectrometers (MS).
The track reconstruction is first independently performed in inner tracker and MS.
The information from both of them is 
then combined to form the muon tracks that are used in the physics analysis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
\subsubsection{Effect of Multiple Scattering}

When a muon passes through the large volume of the materials in the detectors, 
the effect of the multiple scattering needs to be taken into account. 
The multiple scattering angle is regarded as the accumulation of the 
Rutherford scattering.
% which is in inverse proportion to $\sin^4{\left( \frac{\theta}{2} \right)}$, where the $\theta$ 
% is the small scattering angle. 
The probability of single Rutherford scattering is in inverse proportion to $\sin^4{\left( \frac{\theta}{2} \right)}$, where the $\theta$ 
is the scattering angle. The scattering angle has a sharp peak at $\theta = 0$, meaning that $\theta$ is typically very small.
The mean of the multiple scattering angle is statistically regarded as the accumulation of the 
small angle Rutherford scattering, shown as $\displaystyle \langle \theta^2 \rangle =\sum_{i} \theta_i^2$. 
The multiple scattering angle approximately distributes in the Gauss distribution. The effect of the large angle scattering, which also occurs for
$\sin^4{\left( \frac{\theta}{2} \right)}$ distribution
% of the small Rutherford scattering
is shown up in the tail of the distribution. 
The mean of the multiple scattering angle ($\theta_0 \equiv \sqrt{\langle \theta^2 \rangle}$) can be expressed as 
\begin{equation}
\theta_0 = \frac{13.6~\rm{MeV}}{\beta c p} \sqrt{\frac{x}{X_0}} \left[ 1 + 0.038 \ln{\left( \frac{x}{X_0} \right)} \right] \propto \frac{1}{p} \sqrt{\frac{x}{X_0}}, 
\end{equation}
where $p$ and $\beta c$ are the momentum and velocity of a muon, respectively, and $x/X_0$ is the thickness of 
the scattering medium in radiation length ($X_0$). 
If the uncertainty of the muon position measurements, $\sigma_x$ in Eq.~(\ref{eqn:ptreso}) or (\ref{eqn:ptreso2}) 
dominated by the multiple scattering with the detector materials,  the momentum resolution is independent of $\pt$ as 
\begin{equation}
\frac{\sigma_{\pt}}{\pt} \propto \sigma_x \cdot \pt \propto  \theta_0 \cdot \pt \propto  \frac{1}{\pt} \cdot \pt \approx \rm{const.}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Contributions to Muon Momentum Resolution}

The uncertainty of the position measurement $\sigma_x$ usually comes from the accuracy of the hit position
measurement limited by the detector characteristic, misalignment of detectors,  multiple scattering in the detector, 
and fluctuations in the energy loss of the muons traversing through the material in front of the spectrometer. 
Figure~\ref{fig:pTreso_muon} shows the contributions to the momentum resolution for the ATLAS MS  as a function of 
transverse momentum~\cite{ref:MuonTDR}. The contribution of the multiple scattering is independent of the transverse momentum and 
dominated at moderate momentum ($30 < \pt < 300$~GeV), while 
the contributions of the hit position resolution (denoted as "Tube resolution and autocalibration" in Fig.~\ref{fig:pTreso_muon}) and 
the detector (chamber in this case) alignment are in inversely proportion to $\pt$ and dominated at high momentum ($\pt >300$~GeV). 
At low momentum ($\pt <30$~GeV) energy loss fluctuations become dominant.

The ATLAS MS is designed to detect muons in the pseudorapidity region up to $| \eta | = 2.7$, 
and to provide momentum measurements with a relative resolution better than 3\% over a wide $\pT$ range and up to 10\% at 
$\pt \approx 1~\rm{TeV}$. In order to satisfy the requirements, the measurement precision in each hit by a muon track
is required to be typically better than 100~$\mu$m, which can be roughly estimated by Eq.~(\ref{eqn:ptreso}).
The uncertainty of the alignment in the chamber positions is required to be the level of 30~$\mu$m.

\begin{figure}[htbp]
\begin{center}
%\sidecaption
\includegraphics[scale=1.]{muon/reseff15.eps}
%\vspace{10mm}
\caption{Contributions to the muon momentum resolution for the ATLAS MS as a function of transverse momentum~\cite{ref:MuonTDR}~(ATLAS Collaboration {\copyright} 1997 CERN).
}
\label{fig:pTreso_muon}    
\end{center}
\end{figure}

Figure~\ref{fig:pTreso_inner_muon} shows muon momentum resolution for MS alone and for
the combined measurements by MS and inner tracker~\cite{ref:MuonTDR}.
At low momentum ($\pt <30$~GeV), the measurement by inner tracker is better due to better spatial resolution of silicon strip and pixel 
detectors.  On the other hand, at high momentum ($\pt >30$~GeV), the measurement by MS becomes better than inner tracker,
because the MS is stationed  in a wider space, which means $L$ is larger in Eq.~(\ref{eqn:ptreso}). 

\begin{figure}[htbp]
\begin{center}
%\sidecaption
\includegraphics[scale=1.]{muon/inner1.eps}
%\vspace{10mm}
\caption{The muon momentum resolution for the muon spectrometer alone and the combined 
measurements by the ATLAS MS and the ATLAS inner tracker as a function of the transverse momentum~\cite{ref:MuonTDR}~(ATLAS Collaboration {\copyright} 1997 CERN). 
The dashed curve is the resolution using only the inner tracker. }
\label{fig:pTreso_inner_muon}    
\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Examples of Muon Detectors}

Because the muon spectrometers have to cover the wide surface area of the barrel and endcap of the cylindrical detector system,
it is required to be robust, mechanically strong, and inexpensive as well as to provide 
the good momentum resolution and the high efficiency. 
Because muons give us clear signatures from physics of interests such as $H \rightarrow ZZ^* \rightarrow 4\mu$, 
the muon spectrometers are used as the trigger devices which provide fast information on momenta, 
positions and multiplicity of muons traversing through the detector. 
This is called as the first level muon trigger which makes a trigger decision within a few micro-seconds by a simple trigger logic on hardware. 
The gas detectors satisfy %are often satisfied with
these requirements. 
For instance, the ATLAS MS, shown in Fig.~\ref{fig:muon_detector} and Fig.~\ref{fig:cross-section-muon_detector}, 
consists of the resistive plate chambers (RPC) and the thin gap chambers (TGC) to provide 
the fast muon trigger information and the monitored drift tube (MDT) chambers and the cathode strip chambers (CSC) to 
reconstruct muon trajectory precisely. The ATLAS MS divided into a barrel part ($| \eta | < 1.05$)  and two endcaps  ($1.05 < | \eta | < 2.7$). 
%%%
\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.3]{muon/ATLAS_muon.eps}
%\vspace{10mm}
\caption{The muon spectrometer for the ATLAS experiment~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN).}
\label{fig:muon_detector}    
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.29]{muon/ATLAS-muon-rz-tdr-mod2.eps}
%vspace{10mm}
\includegraphics[scale=.2]{muon/newplots_Fig1a.png}
\caption{The cross-section of the ATLAS muon spectrometer: $r$-$z$ view (Left) and $r$-$\phi$ view (Right)~\cite{ref:MuonTDR}~(ATLAS Collaboration {\copyright} 1997 CERN).}
\label{fig:cross-section-muon_detector}       % Give a unique label
\end{center}
\end{figure}

Three large superconducting air-core toroid magnets provide magnetic fields with a bending integral of about 2.5~T$\cdot$m 
in the barrel and up to 6~T$\cdot$m in the endcaps in order to measure the muon momentum independently to the inner tracking system with 
the solenoid magnet (Fig.~\ref{fig:toroid}). In the following sections, as an example of the muon chamber, 
 RPC, TGC, MDT used in ATLAS muon spectrometers, are introduced~\cite{Aad:2008zzm}.


\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.4]{muon/IBdl.eps}
%\vspace{10mm}
\caption{The magnetic fields provided by ATLAS toroid magnet~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN).}
\label{fig:toroid}       % Give a unique label
\end{center}
\end{figure}
%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Resistive Plate Chamber}
In the barrel region~($|\eta| \le 1.05$), trigger signals are provided by a system of resistive plate chambers~(RPCs). 
The RPC is a gaseous parallel electrode-plate detector providing a typical space-time resolution of 1~cm~$ \times$ 1~${\rm ns}$ 
with digital readout. 
The mechanical structure of an RPC is shown in Fig.~\ref{fig:RPC}.
Two resistive plates, made of phenolic-melaminic plastic laminate, are kept parallel to each other 
at a distance of 2~mm by insulating spaces. 
The gas gaps are filled with the gas of a mixture of ${\rm C}_2{\rm H}_2{\rm F}_4/{\rm Iso}$-${\rm C}_4{\rm H}_{10}/{\rm SF}_6$ (94.7/5/0.3).
The electric field between the plates of about $4.9~{\rm kV/mm}$ 
allows avalanches to form along the ionising tracks towards the anode.
Since all primary electron clusters form avalanches simultaneously in the strong and uniform electric field, 
single signal is produced instantaneously after the passages of the particle. 
The intrinsic time jitter is less than 1.5~ns. 
The signal is read out via capacitive coupling to metallic strips, which are mounted on the outer faces of 
the resistive plates. The total jitter of RPC is less than 10~ns, which ensures to identify the proton bunch crossing of 25~ns
and to  produce fast trigger signals. 
The readout pitch of $\eta$ and $\phi$-strips are 23 to 35~mm.
The $\eta$ and $\phi$ strips provide
the bending view of the trigger detector and the second-coordinate measurement,
respectively.
The second-coordinate measurement which cannot be done by MDT chambers
(see \ref{sec:mdt}), 
is also required for the offline pattern recognition.

\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.4]{muon/RPC.eps}
%\vspace{10mm}
\caption{Mechanical structure of an RPC chamber~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN). 
The unit of the number in the figure is mm.
}
\label{fig:RPC}       % Give a unique label
\end{center}
\end{figure}
%%%

RPC is made up of three stations, each with two detector layers. Two
stations installed at a distance of 50~cm from each other are located near the centre of the magnetic
field region and provide the low-$p_{\rm T}$ trigger ($p_{\rm T} > 6$~GeV) while the third station, at the outer
radius of the magnet, allows to detect the muon trajectory with larger curvature 
and to increase the $p_{\rm T}$ threshold to 20~GeV, thus providing the
high-$p_{\rm T}$ trigger.
The trigger logic requires three out of four layers in the middle stations for the
low-$p_{\rm T}$ trigger and, in addition, one of the two outer layers for the high-$p_{\rm T}$ trigger.

\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.4]{muon/RPC_barrel_XY_standard7.eps}
%\vspace{10mm}
\caption{Cross-section of the upper part of the barrel muon spectrometer~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN). 
Two stations of the RPC are below and above 
middle station of MDT chamber. Outer station is above the MDT in the large and below the MDT in the small sectors.
Dimensions are in mm.}
\label{fig:Cross-section-RPCsystem}       % Give a unique label
\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Thin Gap Chamber}
In the endcap region~($1.05 \le |\eta| \le 2.4$), trigger signals are provided by a system of thin gap chambers~(TGCs). 
TGC is multi-wire proportional chambers with the characteristic that the wire-to-cathode distance of 1.4~mm is smaller than 
the wire-to-wire distance of 1.8~mm, as shown in Fig.~\ref{fig:TGC_structure}.
The gas used is mixture of ${\rm CO}_2$ and n-${\rm C}_5{\rm H}_{12}$ (n-pentane) (55 : 45). 
TGC is operational in quasi-saturated mode with a gas gain of about $3 \times  10^{5}$.
The high electric field of the wires (around 2800~V) and small wire-to-wire distance allows us  
 to measure the muon trajectory with a good time resolution and to identify the proton bunch crossing of 25~ns. 
The number of wires in a wire group varies from 6 to 31
as a function of $\eta$, in order to match the granularity to the 
required momentum resolution.  
The wire groups measure the $\eta$ direction of the muon trajectory. 
Two of copper layers in triplet and doublet modules, which is marked as "Cu stripes" in Fig.~\ref{fig:TGC_structure}, 
are segmented into readout strips to read the azimuthal coordinate ($\phi$) of the muon trajectory. 
\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=0.7]{muon/TGC_structure.eps}
\includegraphics[scale=0.6]{muon/TGC_construction.eps}
%\vspace{10mm}
\caption{TGC structure showing anode wire, graphite cathodes, G-10 layers and a pick-up strip, orthogonal to the wires~(top) and 
Cross-section of a TGC triplet and doublet module~(bottom)~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN).}
\label{fig:TGC_structure}   
\end{center}
\end{figure}

The inner wheel formed by doublet modules is placed before the endcap toroidal magnet, 
 while the big wheel consists of the seven layers (triplet module plus two doublet modules) as shown in Fig.~\ref{fig:TGC} and measures
 the muon trajectory in the bending direction by toroidal magnet.
 
\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=0.25]{muon/muon-tgc.jpg}
%\vspace{10mm}
\caption{Big wheel of TGC chamber~\cite{ref:MuonTGC}~(ATLAS Collaboration {\copyright} 2006 CERN). 
The diameter of the Big wheel is about 25~m.
}
\label{fig:TGC}       % Give a unique label
\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Monitored Drift Tube}\label{sec:mdt}

Over most of the $\eta$ range, 
a precise measurement of the track coordinates in the principal bending direction of the toroidal magnetic field is
provided by monitored drift tubes (MDT) chambers.
The MDT system achieves a sagitta accuracy of 60~$\mu$m, corresponding to a momentum resolution of 
about 10\% at $\pt =1~{\rm TeV}$.

The basic element of the MDT is pressurised drift tube with a diameter of 29.970~mm, 
operating with Ar/$\rm{CO}_2$ gas (93\% : 7\%) at 3 bar.
The electrons resulting from ionisation are collected at the central tungsten-rhenium wire 
with a diameter of 50~$\mu$m at a potential of 3080~V as shown in left figure of Fig.~\ref{fig:MDT}. 
The average drift velocity of electrons is about 20.7~$\mu$m/ns and the maximum drift time is about 700~ns.
Making use of the radius-to-drift time relation ($r$-$t$ relation), the distance of a muon track passing through 
in the tube from an anode wire can be measured as a drift circle. 
The shape of the r-t relation, which depends on parameters such as temperature, 
pressure, magnetic field distortions caused by the positive ions after ionisation,
must be known with high accuracy 
in order to achieve better spacial resolution. 

The mechanical structure of a MDT chamber is shown in right figure of Fig.~\ref{fig:MDT}. 
A chamber consists of two multi-layers of three or four drift tube layers.
In order to monitor the internal geometry of the chamber, four optical alignment rays, two parallel 
and two diagonal, are equipped. That is why the drift tube detector in the ATLAS experiment is called "Monitored" Drift Tubes. 
The 1,150 MDT chambers are constructed from 354,000 tubes and cover an area of 5,500~${\rm m}^2$.
Each MDT chamber provides the information of  the track segment. 
Muon tracks are reconstructed by track segments 
obtained from inner, middle, and outer stations of 
%reconstructed by inner, middle, and outer stations of 
MDT chambers.

\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.45]{muon/MDT_tube_cross_section.eps}
\hspace*{5mm}
\includegraphics[scale=.35]{muon/MDT_chamber_schematics_2.eps}
%\hspace*{40mm}
\caption{Left: the cross-section of the MDT drift tube. Right: the mechanical structure of a MDT chamber~\cite{Aad:2008zzm}~(ATLAS Collaboration {\copyright} 2008 CERN).}
\label{fig:MDT}       % Give a unique label
\end{center}
\end{figure}
%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Muon Reconstruction}

The muon reconstruction can be performed independently in the inner tracker and MS.
In the inner tracker the muons are reconstructed such as any other charged particles described in Section~\ref{sec:tracking}.
In this section the description of the muon reconstruction in the MS 
and the combined muon reconstruction are focused on. 
The muon reconstruction at the ATLAS experiment is mainly introduced in Ref.~\cite{Aad:2016jkr}.

Using the drift-circles in MDTs or clusters in TGCs and RPCs, 
the muon reconstruction is subdivided into the 3 stages: 
segment-finding, segment-combining and track-fitting.  

Segment-finding starts with a search for hit patterns in a single station
(i.e. inner, middle, and outer stations of MDT, RPC, and TGC chambers in case of the ATLAS MS)
to form the track segments. 
The Hough transform is used to search for hits aligned on a trajectory in the detector. 
The track segments are reconstructed by a straight-line fit to the hits found in each layer. 

Full-fledged track candidates are built from segments, 
typically starting from middle stations of detector where trigger hits from TGC or RPC are available, 
and extrapolating back through the magnetic field to the segments reconstructed in the inner stations. 
Whenever a match of the segment is found, the segment is added as the track candidate. 
The final track-fitting procedure takes into account all relevant effects: multiple scattering, 
non-uniformity of the magnetic field, inter-chamber misalignment etc.
 
The physics analyses make use of four muon types. 
\begin{itemize}
\item Combined muon: muon tracks reconstructed by the inner tracker and MS independently are combined 
with a global refit using the hits from the inner tracker and MS detectors. In order to improve the fit quality, 
MS hits may be added to or removed from the track. 
Most muon tracks are reconstructed by outside-in reconstruction, where the muons are first 
reconstructed in the MS and then extrapolated inward and match to a track reconstructed by the inner tracker. 
An inside-out reconstruction where the reconstruction procedure is opposite to the 
outside-in reconstruction is also used as a complementary approach. 
\item Segment-tagged muons: a muon track in the inner tracker is classified as a muon if it is associated with 
at least one local segment in the MDT stations. In case of low $\pt$ muon or in case 
muons pass through the $\eta-\phi$ region,
% where some stations of MS  are not covered,
which is not covered by MS stations, 
segment tagged muons are used.
\item Calorimeter-tagged muons: a muon track in the inner tracker is identified as a muon if it is associated with 
an energy deposit in the calorimeter compatible with a minimum-ionising particle~(MIP). 
Muons passing through the $\eta-\phi$ region where MS is not fully covered are regarded as this type of muons.
\item Extrapolated muons: muon tracks reconstructed based only MS and a loose requirement on compatibility with 
originating from the interaction point. 
The tracking parameters of the muon are defined at the interaction point, taking into account the estimated energy loss of the muon in the 
calorimeters. Extrapolated muons are used to extend the acceptance for the muon reconstruction into the region where the inner tracker does not cover.
\end{itemize}

When the same track reconstructed by inner tracker is identified by two muon types, the priority is given to the combined muons, then 
to segment-tagged muons, and finally calorimeter tagged muons. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Muon Identification}

Although muon candidates reconstructed by the muon spectrometers are mostly true muons, 
we want to identify the origin of muons.
Muons from the decay of heavy particles such as $W$, $Z$, Higgs bosons, or new particles 
are interesting for us and need to be reconstructed as "isolated" muons efficiently and precisely. 
Since muons from semi-leptonic decays from $b$ and $c$-hadrons and $\tau$ are also important  
for the $b$-tagging and $\tau$ ID, respectively, they need to be reconstructed as muons in the heavy flavour jets and $\tau$s.  
On the other hand, muons from the decays of pions and kaons are regarded as "fake" muons and eliminated 
from muon candidates.

Muon candidates originating from in-flight decays of charged hadrons, mainly from pion and kaon decays 
in the inner tracker are reconstructed with a distinctive kink in the track. Therefore it is expected 
that the track fit quality of the resulting combined track is poor and that the 
momentum measured by the MS and the inner tracker are not compatible.
Muon identification is performed by applying quality requirements to suppress the background, 
to select prompt muons with high efficiency, and to guarantee a robust momentum measurement.
Based on the number of hits in the inner tracker and MS, $\chi^2$ of the combined muon tracks, 
the difference between the transverse momentum measurements in the inner tracker and MS and their uncertainties 
are used to classify as 'Loose', 'Medium', 'Tight', and 'High $\pt$' (for high momentum muons above 100~GeV 
aimed at the muons from exotic particle such as $Z'$ and $W'$ bosons) categories.
These categories are provided to address the specific needs of different physics analysis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Muon Isolation}

Muons originating from the decay of heavy particles such as $W$, $Z$, or Higgs bosons are often produced isolated from 
the other particles, in contrast to the muons from semi-leptonic hadron decays such as $b \rightarrow c\mu\nu$ which are embedded in jets. 
The measurement of the detector activity around a muon candidate, referred to as muon isolation, is a powerful tool 
for background rejection in many physics analyses.
Both track-based and calorimeter based isolation variables are often used. 

The track-based isolation variable  $p_{\rm T}^{\rm varcone30}$ is defined as the scalar sum of the transverse momentum of the tracks
$p_{\rm T} > 1$~GeV in a cone size $\Delta R =\min{(10~{\rm GeV}/p_{\rm T}^{\mu}}, 0.3)$ around the muon.
The muon momentum $p_{\rm T}^{\mu}$ is excluded from  $p_{\rm T}^{\rm varcone30}$.
In this case, the cone size is chosen either to be $p_{\rm T}$ dependent ($\Delta R = 10~{\rm GeV}/p_{\rm T}^\mu$)
or to be $p_{\rm T}$ independent~($\Delta R = 0.3$). The $p_{\rm T}$ dependent cone size is used 
to improve the performance for the isolated muon with a high transverse momentum.
The calorimeter-based isolation variables 
$E_{\rm T}^{\rm topocone20}$ is defined as the sum of the transverse energy of topological cluster 
in a cone size $\Delta R = 0.2$ around the muon.
The isolation selection criteria are determined using the relative isolation variables defined as $p_{\rm T}^{\rm varcone30}/p_{\rm T}^{\mu}$ 
and $E_{\rm T}^{\rm topocone20}/p_{\rm T}^{\mu}$. 
Several selection criteria are % optimised and
provided to address the specific needs of different physics analyses.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Momentum Scale and Resolution}

Although the simulation contains %an accurate 
the description of the detector, 
there is a limitation in describing the momentum scale
%which is  the offset of the centre value of the momentum measurement in simulation to data, 
and the momentum resolution.
%which is the difference of the resolution estimated by simulation from one by data.
For this reason, corrections of simulated values are often applied.
The momentum scale and resolution are parametrised by the following equation:
\begin{equation}
p_{\rm T}^{\rm Cor} = \frac{\displaystyle p_{\rm T}^{\rm MC} + \sum^{1}_{n=0}s_n(\eta, \phi )\times \left(p_{\rm T}^{\rm MC}\right)^n}{\displaystyle 1+  \sum^{2}_{m=0}\Delta r_m(\eta, \phi ) \times \left(p_{\rm T}^{\rm MC}\right)^{m-1} g_m}
\label{muon_momentum_corr}
\end{equation}
where $p_{\rm T}^{\rm MC}$ is the uncorrected transverse momentum in simulation, $g_m$ is normally 
distributed random variables with zero mean and unit width, and the $\Delta r_m(\eta, \phi)$ and $s_n(\eta, \phi)$
are the parameters representing the smearing of momentum resolution and the scale corrections applied in a specific $(\eta, \phi)$ 
detector region, respectively.  

%The momentum scale which is offset of the momentum in data to one in simulation

The corrections to the momentum resolution are described by
the denominator of Eq.~(\ref{muon_momentum_corr}), assuming that the relative $p_{\rm T}$ resolution can be parameterised by 
\begin{equation}
\label{momentum_resolution}
\frac{\sigma{(p_{\rm T})}}{p_{\rm T}} = \frac{r_0}{p_{\rm T}} \oplus r_1 \oplus r_2 \cdot p_{\rm T}
\end{equation}
with $\oplus$ denoting a sum in quadrature. 
As shown in Section~\ref{Basics_muon_momentum_measurement}, 
the second and third terms of Eq.~(\ref{momentum_resolution}) account mainly for multiple scattering and 
the resolution effects caused by spatial resolution of the hit measurements and the misalignment of the muon spectrometer. 
The first term accounts for fluctuation of the energy loss in the detector material.
The difference of the momentum resolution between data and simulation is parametrised by $\Delta r_m(\eta, \phi)$.
The momentum in simulation is smeared with the $\Delta r_m(\eta, \phi)$, by dividing uncorrected muon momentum 
by the term of denominator in Eq.~(\ref{muon_momentum_corr}). 

The numerator in Eq.~(\ref{muon_momentum_corr}) describes the momentum scales. 
The $s_1(\eta, \phi)$ corrects for inaccuracy in the description of the magnetic field integral 
and the dimension of the detector in the direction perpendicular to the magnetic field. 
The $s_0(\eta, \phi)$ corrects the energy loss in the detector material.
%By adding $\displaystyle \sum^{1}_{n=0}s_n(\eta, \phi )\times \left(p_{\rm T}^{\rm MC}\right)^n$ to the momentum 
%in simulation, it is corrected.

The momentum scale and resolution are usually studied using $J/\psi \rightarrow \mu \mu$ and 
$Z \rightarrow \mu \mu$ decays. 
Since the $J/\psi$ and $Z$ are narrow resonances and their masses are well-known, 
the distributions of invariant mass reconstructed by two $\mu$'s from $J/\psi$ and $Z$ show
clear peaks around 3~GeV and 91~GeV~\cite{Zyla:2020zbs}, respectively. Furthermore, the number of 
non-resonant background events from decays of light and heavy hadrons and from 
continuum Drell-Yan production is very small.
The momentum scale and resolution are determined from data using a fit with templates derived from 
simulation which compares the invariant mass distributions from $J/\psi \rightarrow \mu \mu$ and 
$Z \rightarrow \mu \mu$ candidates in data and simulation. 
The momentum in the range of 5~GeV$<p_{\rm T}<$20~GeV and 20~GeV$<p_{\rm T}<$300~GeV are 
 corrected by $J/\psi \rightarrow \mu \mu$ and $Z \rightarrow \mu \mu$ candidates, respectively.
%The muon momenta are corrected using the parameters obtained by the fit.
Figure~\ref{fig:dimuonmass} shows the invariant mass distribution of $J/\psi \rightarrow \mu\mu$ (left) and 
$Z \rightarrow \mu\mu$ (right) candidate events reconstructed with combined muons~\cite{Aad:2016jkr}.
The agreement between data and simulation becomes much better after the correction. 
\begin{figure}[htbp]
\begin{center}
\sidecaption
\includegraphics[scale=.28]{muon/dimuonmass_jpsi.eps}
\includegraphics[scale=.28]{muon/dimuonmass_Z.eps}
%\hspace*{40mm}
\caption{Dimuon invariant mass distribution of $J/\psi \rightarrow \mu\mu$ (left) and 
$Z \rightarrow \mu\mu$ (right) candidate events reconstructed with combined muons~\cite{Aad:2016jkr}~(ATLAS Collaboration {\copyright} 2017 CERN).
The upper panels show the invariant mass distribution for data and for the signal simulation, and 
for background estimate. The points show the data, the continuous line show the simulation with the 
corrections of momentum scale and resolution, and the dashed lines show the simulation 
without the corrections.
}
\label{fig:dimuonmass}       % Give a unique label
\end{center}
\end{figure}
%%%
%\input{muon/ref_muon}
