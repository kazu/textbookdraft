%%%%%%%%%%%%%%%%%%%%% chapter.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sample chapter
%
% Use this file as a template for your own input.
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{$H \rightarrow W^\pm W^{\mp\ast}$}
%Measuring production cross section of Higgs bosons decaying to a $W^+W^-$ pair}
\label{hww}

\subsubsection{Analysis Overview}

The branching ratio of the decay channel $h \rightarrow W^+W^-$ is
about 22\%, which is the second largest for $m_h = 125$\,GeV.
Since the mass of the Higgs boson is less than the sum of two
$W$ boson masses (about 161\,GeV), one of the two $W$ boson decays
virtually ($h \rightarrow WW^\ast$). The analysis using 8\,TeV data
from the ATLAS collaboration is described in detail
in Ref.~\cite{ATLAS:2014aga}. The corresponding
13~TeV analysis is given in Ref.~\cite{ATLAS:2018xbv}; there
the data analysis procedure is given briefly and
refers to the former paper~\cite{ATLAS:2014aga}. In this section
some key points of the $H \rightarrow WW^\ast$ analysis are described.
%For detail please refer to the 8\,TeV paper.


The analysis uses the leptonic decay channel for both of
the $W$ bosons ($h \rightarrow WW^\ast \rightarrow \ell \nu \ell \nu$)
(Fig.~\ref{fig:EWWW}(a)), where
$\ell$ is either an electron ($e$) or a muon ($\mu$) in order
to reduce background from multijet production $pp \rightarrow jets$.
Since the multijet final state can be produced with process with
only QCD vertices with strong coupling,
the cross section of such production is many order of magnitudes
larger than the that of $h \rightarrow WW^\ast$ signal.
The decay product of the Higgs boson, therefore, are either of
$ee, e\mu, \mu\mu$ combinations with two or more neutrinos.
The analysis also includes
smaller number of events containing $W \rightarrow \tau \nu_\tau$ decays
where the $\tau$-lepton further decays into an electron or muon with
two additional neutrinos.
Only
the sum of the transverse momenta of the neutrinos (here denoted
as $\vec{p}_{\rm T}^{\nu\nu}$) can be measured
through missing $\vec{p}_{\rm T}$.

\begin{figure}
\begin{center}
\includegraphics[scale=.45]{Higgs/HWW/fig8_x_irreducible.png}
\caption{The diagrams of (a) $h\rightarrow WW$
(b) a $WW$-pair production through a $Z^0$ and
(c) a top-quark pair with both $W$ bosons decaying leptonically
into $e\mu$.}
\label{fig:EWWW}
\end{center}
\end{figure}

Major source of the background are resonant-like $WW$ production
(Fig.~\ref{fig:EWWW}(b))
and top-pair events where both top quarks decay leptonically,
$t \rightarrow Wb, W \rightarrow \ell\nu$ (Fig.~\ref{fig:EWWW}(c)).
The former process
has the same final state as the signal and is an irreducible
background source if the $WW$ pair is produced from a colourless state such as
a virtual $Z^0$ boson.
%The latter process gives two $b$-jets and is the $\ast$ main
%background for VBF $\ast$. 
The latter process gives two $b$-jets and is the main background for VBF
production process where we require two jets in the final state,
and also significant source for the events with one jet in the final state.

The reconstruction of the Higgs boson mass is not possible because of
the neutrinos in the final state.
Instead, the transverse mass, $m_{\rm T}$, is calculated
to estimate the invariant mass of the $WW^\ast$ system, which uses
the transverse components of the kinematic variables: $p_{\rm T}^{\nu\nu} (p_{\rm T}^{\ell\ell})$,
the vector sum of the neutrinos (leptons) and
$E_{\rm T}^{\ell\ell} = \sqrt{(p_{\rm T}^{\ell\ell})^2 + (m_{\ell\ell})^2}$.
The $m_{\rm T}$ is defined as 

\[
m_{\rm T} = \sqrt{(E_{\rm T}^{\ell\ell}+p_{\rm T}^{\ell\ell})^2
      - |\vec{p}_{\rm T}^{\ell\ell} + \vec{p}_{\rm T}^{\nu\nu}|^2} .
\]

Since $m_{\rm T} \leq m_h$ and $m_h$ is below twice the $W$ boson mass,
$h \rightarrow WW^{\ast}$ events will be populated in $m_{\rm T}$ region
below that from resonant $WW$ production (see Fig.~\ref{fig:hWWmT-jetcategories}.)
The $m_{\rm T}$ values
for top-pair production also tend to be much beyond that from
the Higgs decays. This shape difference is used to quantitatively
distinguish the signal and background. The peak structure in $m_{\rm T}$,
for both signal and the $WW$ background, however, are broad. Also
the production rate of $WW^*$ pairs is much smaller than the SM diboson
production. Several other feature of the signal events are used
to reduce background processes.

The number of jets, especially the number of $b$-jets, is one of such key
ingredients to classify event categories. As described
in Section~\ref{sec:HiggsProductionProcess}, at the leading order
there is no jet for the $ggF$ processes,
while in the $VBF$ processes each of two incoming quarks
emits a vector boson and recoiled, giving two
jets close to outgoing beam direction, one for each side. This means
that two forward jets are observed, with large separation in rapidity space.
Since these forward jets in the VBF processes are jets from light
quarks, the background from $t\bar{t}$ production is greatly suppressed
by removing events with one or more $b$-quark jets.

The azimuthal correlation of the two leptons is also used
in order to further enhance the signal. Since the Higgs boson
is a scalar particle and has no spin, the spin directions
of the two $W$ bosons are opposite (Fig.~\ref{fig:hWWspincorr}(a)).
The momentum direction of the charged
leptons in $W^- \rightarrow \ell^-\bar{\nu}$ decays tend to be opposite
to the spin direction since the anti-neutrino is right-handed and
its momentum is aligned to the spin direction. For the $W^+$ the charged
lepton is emitted along the direction of the $W^+$ spin.
As the $WW$ pairs tend to have back-to-back topology in $x-y$ plane,
the direction of the two leptons becomes close as shown in Fig.~\ref{fig:hWWspincorr}(b). Also the invariant
mass of the lepton pair, $m_{\ell\ell}$, is peaked around 30 -- 40\,GeV
while for $WW$ pair production it is at around 60 GeV, as seen in
Fig.~7(b) in Ref.~\cite{ATLAS:2014aga}.

\begin{figure}[h]
\centering
\hspace*{1cm}
\includegraphics[width=10cm]{Higgs/HWW/HiggsAngles.png}
\caption{(a) Relation between the spin direction and momentum direction
for $H \rightarrow WW^\ast \rightarrow \ell\nu\ell\nu$ decays.
(b) Illustration of typical decay topology in $x-y$ plane (perpendicular
to the beam direction).
}
\label{fig:hWWspincorr}
\end{figure}

Since the signal-to-background ratio is quite small in $WW^\ast$
decay channel, the amount of the remaining background is still very
large after selecting Higgs-like events using the properties given
above. The remaining background depends strongly on the number
of accompanied jets. The events are, therefore, classified according
to the number of jets: 0-jet, 1-jet and $\geq 2$-jet categories. The main
background sources for the 0-jet category are irreducible $WW$ production
and other diboson production, especially $WZ$ events where one
of the lepton is missed. In addition the events from
$W + {\rm jets}$ production contributes significantly if the jet
is misidentified as a lepton. Here the $W + {\rm jets}$ process
represents higher-order DY events $q\bar{q} \rightarrow W^{\pm}$, i.e.
with one or more associated jets.
For 1-jet category the $t\bar{t}$ production becomes
also significant since it produces two $b$-jets where one of
the jets is experimentally not tagged as a $b$-jet. For the 2-jet
events the major contribution is the $t\bar{t}$ events.
Basic idea of how to suppress these background events is
described in the next subsection for each category.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Background Reduction}

\begin{itemize}

\item 0-jet category
\begin{quotation}
After the basic requirement of having two leptons in the final state,
significant missing $E_{\rm T}$ and explicitly requesting no jet,
most of the background is the DY process,
$pp \rightarrow Z^0/\gamma^\ast + X, Z^0/\gamma^\ast
\rightarrow ee, \mu\mu, \tau\tau$,
especially when the two leptons have the same flavour ($ee$ or $\mu\mu$).
This background is also significant for $e\mu$ channel, however,
since both the $\tau$ leptons in $pp \rightarrow \tau^+\tau^- X$
processes may decay leptonically, giving a $e\mu$ pair.

In order to further reduce the DY events,
the correlation of the lepton pair is used, by requiring $p_{\rm T}$ of
the dilepton system being high: $p_{\rm T}^{\ell\ell} > 30$\,GeV
(Fig. 7(a) in Ref.~\cite{ATLAS:2014aga}). Since the $Z^0/\gamma^\ast$
in the DY processes are produced from $q\bar{q}$ annihilation, each
of the quarks coming from the incoming protons, the transverse momentum
of the produced $Z^0/\gamma^\ast$ tend to be small and the lepton pair from the decay
tends to be produced back-to-back in the $x-y$ plane.

The missing $E_{\rm T}$ may arise from background processes through
mismeasurement of the energy or momentum of the final state
particles, i.e. two leptons.
In such cases the missing $E_{\rm T}$ tends to be aligned to
the momentum direction of these particles. A few requirements
are applied based on the relative momentum of the missing $\vec{p}_{\rm T}$
to the leptons.

Finally, the azimuthal correlation requirement $(\phi_{\ell\ell} < 1.8)$
and the mass of the dilepton system $m_{\ell\ell} < 55$\,GeV are
required to select events with $H \rightarrow WW^\ast$ topology as described
above.
\end{quotation}

\item 1-jet category
\begin{quotation}
The event selection for the 1-jet category is very similar
to that for the 0-jet events apart from a few points: the required
jet should not be tagged as a $b$-jet; 
$\vec{p}_{\rm T}^{\ell\ell}$ is replaced to
$\vec{p}_{\rm T}^{\ell\ell j}$, adding the momentum of the jet;
and additional requirement on the $m_{\tau\tau}$ variable
is imposed: $m_{\tau\tau} < m_Z - 25$\,GeV where $m_Z$ is the mass
of the $Z^0$ boson. The $m_{\tau\tau}$ variable is calculated by
using so-called ``collinear approximation" assuming that the leptons
are from the decay of $\tau$ leptons originated from $Z^0$ and
the momentum of the rest of the $\tau$ decay products, two neutrinos
for each decay, are estimated by projecting the missing $\pt$ vector
to the two lepton directions.
\end{quotation}

\item 2-jet category
\begin{quotation}
The signal-to-noise ratio for two-jet VBF categories is
much smaller than the other categories at the stage after
dilepton + missing $E_{\rm T}$ selection. In order to enrich
the signal, a machine-learning technique (boosted decision tree,
BDT) is used. The detail of the technique is beyond the scope
of this book. Here we merely explain the main variables
used as inputs for the machinery. Two variables related to
the forward-going two jets, the jet-jet mass $m_{jj}$ and the rapidity
difference between the two jets $y_{jj}$,
play main role in the selection since the two jets in the VBF
process tend to have large values. Some other variables related to 
the angular order of the VBF jets and the decay products of the Higgs boson are
used to enrich the VBF process, based on the fact that 
the Higgs boson is produced
in between the two jets, each of which goes into near the outgoing
beam direction on the opposite sides (see Fig.~\ref{fig:HiggsProd}(b)). 
In addition, since
the VBF is a quark induced process without QCD vertex~(see Section~\ref{sec:HiggsProductionProcess}), the amount
of the initial and final state radiations from partons are largely
suppressed with respect to the main background process, 
the $t\bar{t}$ production. The vector
sum of $\vec{p}_{\rm T}$ over hard objects in an event is
sensitive to the amount of such radiation since the size of such
vector indicates the amount of recoil received by the objects.
% For more detail please refer to the 8\,TeV paper\cite{ATLAS:2014aga}.
\end{quotation}

\end{itemize}

% result of the selected events, 13 TeV figures?

Figure~\ref{fig:hWWmT-jetcategories} show the $m_{\rm T}$ distribution
of the events after all the selection for $e\mu$ channel. 
A clear excess over the sum
of the background is observed for both 0- and 1-jet categories.
%%もしかして書いてる途中？？？
The amount of the excess divided by the expected number of events
predicted by the Standard Model Higgs boson production cross section
is called signal strength parameter (denoted as $\mu$). The value of
$\mu$ is extracted from the fit to $m_{\rm T}$ distributions of all
the event categories after fixing the background distributions including
their normalisations, as described below. 
% The method to estimate background events is described in the next
% section.
\begin{figure}
\begin{center}
\includegraphics[height=50mm]{Higgs/HWW/fig_11a.png}
\includegraphics[height=50mm]{Higgs/HWW/fig_11c.png}
\caption{Distributions of $m_{\rm T}$ for 0 and 1-jet events selected
for $ggF$ signal (Fig. 13 (a) and (c) in Ref.~\cite{ATLAS:2014aga}~(ATLAS Collaboration {\copyright} 2014 CERN))}
\label{fig:hWWmT-jetcategories}
\end{center}
\end{figure}
%% なぜか図が見えない。

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% introduction to background estimation

\subsubsection{Background Estimation}

It is difficult to determine the amount of background
events through template fit assuming the shape of the signal and background
and determining the normalisation of each contribution through the fit, 
since the $m_{\rm T}$ distribution for signal is relatively broad
and the shape is somewhat similar for the signal and some of the
background events as seen in Fig.~\ref{fig:hWWmT-jetcategories}.
The background contribution is therefore estimated by using event
distributions in control regions where some of the selection criteria
are inverted so that there is no overlap in events between the signal
and control regions.

In this analysis, the control regions are prepared for each
process for each category of events (0, 1 or 2 jets, $e\mu$ or $ee + \mu\mu$
final states) and for each background processes ($WW$, top, Drell-Yan etc.).
Instead of going through all of them, we pick up a few 
most relevant ones.

For example, the normalisation of the $WW$ contribution is obtained
by events in high $m_{\ell\ell}$ region, $55 < m_{\ell\ell} < 110$\,GeV
for the 0-jet category so that the purity of the $WW$ contribution 
is improved, while keeping similar event selection criteria 
to the signal region.
%if not exactly the same
The remaining background sources from non-$WW$ processes in this
control region are subtracted by using simulated events.

%%%%%
% Comment by Hanagaki: まだ書いてる途中？
The strongest constraint for normalising $t\bar{t}$ contribution
comes from 1-jet category $e\mu$ final state, but requesting
one $b$-tagged jet explicitly, since all top quarks practically decay 
to the $bW$ final state. In addition, the requirement on
lepton is tightened by requesting $m_{\rm T}^\ell > 50$\,GeV, where
$m_{\rm T}^\ell$ is defined as the mass between one of the leptons
and missing $\pt$ vector on the $x-y$ plane. It is meant
for reconstructing the transverse mass of the $W$ bosons from
the top-quark decays. After applying these criteria,
the control region consists almost fully of top quark production.
Thus determined background fraction gives consistent result with simulation
for most of the control regions, despite that
the event selection for $H \rightarrow WW$ may be at the corner of
the phase space for the background processes. 
%See Fig.\,21 of the 8TeV paper.

After repeating similar exercise for other event categories, the normalisation
factors for the background processes as well the signal contribution are
finally fixed by performing a simultaneous likelihood fit, where some 
of the normalisation
factors are allowed to shift while others are fixed. The final result
for the 8\,TeV analysis gives 
$\mu = 1.09^{+0.16}_{-0.15}{\rm (stat)}^{+0.17}_{-0.14}{\rm (syst)}$. 
The main sources of the systematic uncertainties are theoretical origin, 
like the cross section prediction of the signal itself, since the strength
parameter is the cross section ratios of measurement to prediction. For
the 13\,TeV analysis, the statistical uncertainty was improved and became
lower than the total systematic uncertainties.   
% (see Fig.\,28
%of the 8 TeV paper). The post-fit result is shown in Fig.\,35 in
%the 8 TeV paper and Fig\,8 in the 13 TeV paper for the sum of
%0- and 1-jet categories.

% Firstly, events 
% without jet requirement i.e. events containing any number of jets
% are selected and the probability to have zero jet is estimated using
% this sample. There needs a correction factor to take into account
% the systematic shift ....


% WW: using CR, all BG subtracted using MC and the number of CR WW contribution
% estimated by remaining amount of the data.
% The extrapolation factor B_SR/B_CR is estimated using MC (B_xx represents
% number of events obtained from the MC

% Top: the same method as WW
% Need to explain the definition of CR how and why

% diboson: for emu it is normalisation method (the same as WW)
% but for others we use MC.

%\input{referenc_hWW}
