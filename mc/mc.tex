%!TEX root = ../book.tex
\chapter{Event Simulation}
\label{chap:mc_simulation} 

Not only real experimental data but also simulated data are necessary for modern data analyses
because experiments, that is, detectors etc. are getting complex so that we need help of Monte Carlo~(MC) simulation to understand the experimental data.
A {\it Monte Carlo} method is a technique to simulate high-energy physics interactions and detector responses by applying random samplings to probability distributions modelling experiments.
In collider experiments, MC simulated events~(MC events or MC samples in short) are produced event-by-event;
in case of proton-proton colliders each event corresponds to a bunch crossing, where several $pp$ collisions~(pile-up) may occur.
%Studies with Monte Carlo~(MC) simulated events are necessary for data-analysis of searches and measurements in particle physics experiments.
%A {\it Monte Carlo} method is a technique to simulate what we want to investigate by applying random samplings to probability distributions modeling our experiments.
%In particle physics experiments, MC simulated events~(MC events or MC samples in short) are produced event-by-event.
MC simulation is also useful to design new experiments and detectors.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Overview}
\label{mc_sec:overview}
%There are different types of MC simulation: {\it full} simulation, {\it fast} simulation, {\it parametric} simulation and so on\footnote{The terminology to specify the type of MC simulation is not unique but depends on experiments. The machine learning techniques are also used for the MC simulation, for example, GAN.}.
%We outline the production of MC events with the {\it full} MC simulation, where three steps are considered: event generation, detector simulation and reconstruction as shown in Fig.~\ref{fig:MCSimFlow}. 
We outline the production of MC events with MC simulation, where three steps are considered: event generation, detector simulation and reconstruction as shown in Fig.~\ref{fig:MCSimFlow}. 
%Other simulations are explained in Section~\ref{mc_sec:detector-simulation}.

In the event generation step, we produce events, for example, two photons from Higgs bosons in proton-proton collisions~($pp\rightarrow gg\rightarrow H\rightarrow \gamma\gamma$), or two quarks from $Z$ bosons in electron-positron collisions~($e^+e^-\rightarrow Z\rightarrow q\bar{q}$).
Unstable particles, whose lifetime is short enough not to reach detectors, are decayed according to branching fractions which are obtained from experimental measurements or theoretical predictions. The output of this step is a list of particles with various information, for instance, energy, momentum, production and decay positions, status and relation between particles, {i.e.} parent and children.

In the detector simulation step, we simulate our detector responses to the stable particles produced in the previous step.
For example, in case of electrons, they lose energy by interacting with detectors: produce electron-hole pairs in pixel and silicon detectors, ionise particles in gas detectors, produce EM showers in calorimeters. 
These energy deposits are converted to charges if necessary.
The format of outputs is the same as that of the real-data from detectors because the next step should be applied to both real-data and MC simulation.
The detector simulation should ideally be as precise as possible but it depends on the requirements of physics achievements and the technical limitations, for example, modelling of detectors, computing resources etc. 
%The meaning of a term of {\it full} is explained in Section~\ref{mc_sec:detector-simulation}.
The pile-up effect in the $pp$ collisions can be taken into account after the detector simulation, for example, we prepare several events of the inelastic interactions and mix them following the number of collisions per bunch crossing.

In the reconstruction step, we reconstruct events from the output of the detector simulation and identify particles.
Reconstructed objects in each event are still {\it candidates} of particles. For instance, electron {\it candidates} mean that they are reconstructed from EM calorimeter clusters matched with charged tracks. They come from electrons~(called true electrons) or fake electrons~($\pi^\pm$, $\tau$ etc.); note that the fraction of true electrons is not so high.
Then, identification programs are applied in order to select, for example, true electrons from electron {\it candidates} as much as possible.
In other words, fake electrons are rejected as much as possible.
As a result, the fraction of true electrons in the selected electron {\it candidates} becomes high.
Momentum and energy are also calculated for each object including calibrations if possible. 
The output of this step is used in the data-analysis.

%\begin{figure}[b]
\begin{figure}
\sidecaption
\includegraphics[scale=.85]{mc/figs/MCSimFlow.eps}
\caption{The flow of the production of MC events~({\it full} detector simulation) and real-data for data-analysis.
The {\it Monte Carlo} method is used in the event generation and detector simulation steps but not in the reconstruction step.
The reconstruction step should work for both MC events (solid) and real-data (dashed).
}
\label{fig:MCSimFlow}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Event Generation}
\label{mc_sec:event-generation}
An MC event is produced with several steps, where each step uses different programs.
We explain the outline of how to produce an event using a concrete example of the $t\bar{t}$ process in $pp$ colliders
with several keywords often used for MC production: matrix-element event generator, parton density function, parton shower, fragmentation, harmonisation, underlying event etc.
The detail of theoretical aspects can be found in books, for example, Ref.~\cite{Ellis:1996mzs}.
Then, we give concrete computing programs used in the ATLAS experiment.

%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Production of $t\bar{t}$ Process}
Let us consider the production of $t\bar{t}$ events in $pp$ collisions: $t\bar{t}, t\rightarrow W^+b, \bar{t}\rightarrow W^-\bar{b}$ where one of $W$ bosons decay into quarks and the other into leptons.
Figure~\ref{fig:MCgg2tt} shows two Feynman diagrams of this production: $gg\rightarrow t\bar{t}, t\rightarrow W^+b, W^+\rightarrow u\bar{d}, \bar{t}\rightarrow W^-\bar{b}, W^-\rightarrow \ell^-\bar{\nu}$. This is so-called a hard-process part.
Matrix-element event generators (ME generators) are used for the production of the hard-process part.
ME generators can produce events by considering all the diagrams which have the same final state.
ME generators also perform the simulation of a gluon~($g$) from a proton in the proton-proton collision.
The momentum fraction of gluons is described by parton density functions~(PDF), which are obtained based on QCD and experimental measurements.
Gluon and quark PDFs depend on the energy of the interaction and we need to define an energy scale to evaluate PDFs, for example, as top mass for $t\bar{t}$ production.
This scale is one of the important parameters in the MC production, which is called a factorisation scale $\mu_F$.

\begin{figure}
\sidecaption
\includegraphics[scale=.4]{mc/figs/gg2ttbar-2a.eps}
\includegraphics[scale=.4]{mc/figs/gg2ttbar-2c.eps}
\caption{Feynman diagrams of $t\bar{t}$ process in the $pp$ collisions: $gg\rightarrow t\bar{t}, t\rightarrow W^+b, W^+\rightarrow u\bar{d}, \bar{t}\rightarrow W^-\bar{b}, W^-\rightarrow \ell^-\bar{\nu}$ with $s$-channel~(left) and $t$-channel~(right).}
\label{fig:MCgg2tt}
\end{figure}

However, the production of a hard-process using ME generators is not the end of story but a starting point of the event generation.
To produce MC events, several different steps~(parton shower, fragmentation/hadronisation etc.) have to be performed as shown in Fig.~\ref{fig:MCgg2ttDetail}.
Lots of quarks and gluons with relatively small \pT\ are emitted using a method of parton shower, that is, soft and collinear emissions.
A Sudakov form factor, which is a probability not to emit a parton until a target energy scale, is calculated to perform the MC method.
There are two different types of showers for such emissions: initial state radiation~(ISR) and final state radiation~(FSR);
the theoretical idea behind them is similar, however, ISR is complicated due to so-called backward evolution,
which is ``tracing the showers backwards in time'' (see, for example, Ref.~\cite{Sjostrand:2006za}).
As shown in Fig.~\ref{fig:MCgg2ttDetail}, the use of ISR and FSR depends on when the emissions happen; ISR~(FSR) is for radiations before~(after) the hard-process.
ISR gluons and quarks are radiated from the initial gluons of $gg\rightarrow t\bar{t}$ but
FSR gluons are radiated from quarks~($u$ and/or $\bar{d}$) of $W^+\rightarrow u\bar{d}$.

\begin{figure}[htbp]
\sidecaption
\includegraphics[scale=.45]{mc/figs/gg2ttbar-complex}
\caption{Details of MC event production of $t\bar{t}$ from the proton-proton collisions: parton showers~(ISR, FSR), fragmentation, hadronisation, photon radiation and underlying event. The hard-process shown with a dashed box is the same as the left plot of Fig.~\ref{fig:MCgg2tt}. This figure is based on Fig.~1 of Ref.~\cite{Gleisberg:2008ta}.} 
\label{fig:MCgg2ttDetail}
\end{figure}

Quarks and gluons cannot be observed due to the colour confinement (see Section~\ref{sec:jetID}), so that they need to be combined to compose hadrons.
This procedure is called hadronisation.
For example, to produce a $B$ meson~($B^0(\bar{b}d), B^+(\bar{b}u)$), $u$ or $d$ quarks are produced from parton shower/fragmentation and one of them is combined with a $\bar{b}$ quark.
New additional quarks and gluons are created from the existing quarks, gluons, or vacuums, which are gluon fields in this case. 
This step is called fragmentation but sometimes included in the step of hadronisation.
Photons can be radiated from charged leptons and quarks, which are called QED radiative corrections.
Remaining parts of protons not used in the hard-process are called ``underlying event'' and must be treated properly with parton showers, hadronisation and fragmentation.
Finally the decay of mesons, baryons and leptons is performed until particles produced from decays become ``stable''.
Kinematics~(energy and momentum) of all the particles are determined under conservation rules~(energy, momentum, spin/polarisation etc.) with the MC method.

Feynman diagrams shown in Figs.~\ref{fig:MCgg2tt} are the leading-order~(LO) of $gg\rightarrow t\bar{t}$ process and there is no additional gluons and quarks.
However we can use ME generators to produce more high \pT\ gluons and quarks. For example, when we consider one additional strong coupling,
additional gluon or quark can be emitted, which should be treated by the ME generators instead of the parton shower.
This is a part of the contribution of next-to-leading order~(NLO).
Gluons or quarks produced by ME generators and by parton shower are properly treated to avoid a double counting, which is briefly explained in the next section.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Event Generators}
Many event generators are available on the market. Several main generators used in ATLAS for Run~1 and Run~2 data-analysis are listed with alphabetical order: 
\textsc{Alpgen}~\cite{Mangano:2002ea}, \textsc{Herwig}~\cite{Bahr:2008pv,Bellm:2015jjp,Corcella:2000bw}, \textsc{MadGraph}~(\textsc{MadGraph5\_aMC@NLO})~\cite{Alwall:2014hca},
\textsc{MC@NLO}~\cite{Frixione:2002ik}, \textsc{Photos}~\cite{Golonka:2005pn}, \textsc{Powheg}~\cite{Nason:2004rx},
\textsc{Pythia}~\cite{Sjostrand:2006za,Sjostrand:2007gs}, \textsc{Sherpa}~\cite{Gleisberg:2008ta}, \textsc{Tauola}~\cite{TAUOLA} etc.
Since different generators have different features, each generator has their own pros and cons.

\textsc{Herwig}, \textsc{Pythia} and \textsc{Sherpa} are multi-purpose event generators.
They can do all the steps explained before including parton showers, fragmentation, hadronisation and decay.
%Not only leading-order~(LO) but also next-to-leading order~(NLO) calculations for ME are available for a part of processes,
Not only LO but also NLO calculations for ME are available for a part of processes in these generators. 
However, other ME generators like \textsc{MadGraph}, \textsc{Powheg} etc. are often used for NLO in ATLAS.
\textsc{Herwig} and \textsc{Pythia} can be used for simulating parton showers, fragmentation, hadronisation and decay in these ME event generators.
The theoretical idea of the parton shower is different among generators: angular ordering for \textsc{Herwig} and \pT~(or $k_T$) ordering for \textsc{Pythia}.
Models for fragmentation and hadronisation are also different: cluster model for \textsc{Herwig} and string model for \textsc{Pythia}.
Their difference is often used as systematic uncertainties from the parton shower and fragmentation/hadronisation models.
\textsc{Herwig} has three major versions; \textsc{Herwig}, \textsc{Herwig++} and \textsc{Herwig~7}.
\textsc{Pythia} has two major versions: \textsc{Pythia~6} and \textsc{Pythia~8}.
\textsc{Herwig++}, \textsc{Herwig~7} and \textsc{Pythia~8} were often used in ATLAS in Run~2 comparing to \textsc{Herwig} and \textsc{Pythia~6} since new features and techniques of event generations were only implemented in \textsc{Herwig++}, \textsc{Herwig~7} and \textsc{Pythia~8}.

\textsc{Alpgen}, \textsc{MadGraph} and \textsc{Sherpa} are multi-leg generators.
They can produce events with ME including multi-partons.
The multi-partons are so-called additional partons or additional jets, so that we often call such physics processes $X$+jets, 
for example, $W$+jets, $Z$+jets etc.
The idea behind such additional jets with ME~(ME jets) is that the modelling of jets produced with parton showers~(PS) might not work well
because the parton shower is based on soft and collinear approximation.
Figure~\ref{fig:W_2jets} shows one of diagrams for $W$+2-jets and these additional quark and gluon associated with a $W$ boson can be produced by either ME or PS.
Calculations based on ME is in principle correct (see also the next paragraph, however);
when the existence and behaviour of additional jets are critical in data-analysis,
the use of ME generators is recommended to describe additional jets, in particular, high \pT\ jets, but we should be aware that the implementation of loop-corrections etc. may depend on each generator.
For example, in the SUSY searches, typical SUSY events from gluinos and squarks have several jets
in the final state (ex. $gg \rightarrow \tilde{g}\tilde{g} \rightarrow qq\tilde{\chi_1}^0 qq\tilde{\chi_1}^0$) and 
one of dominant background processes is $Z \rightarrow \nu\bar{\nu}$+jets. 
The ``+jets'' of the $Z$ process must be modelled well to predict background, so that \textsc{Sherpa} is used for $Z$ plus up to 4-jets with ME in ATLAS~\cite{ATLASPUBVJETS}.

\begin{figure}
\sidecaption
\includegraphics[scale=.60]{mc/figs/gq2W_jets.eps}
\caption{Feynman diagram of $W$+jets: 2 jets (a quark and a gluon) are associated with a $W$ boson.}
\label{fig:W_2jets}
\end{figure}

There is one important thing to be considered, which is called a jet-parton matching.
Even if multi-leg generators are used to produce additional jets with ME, the parton shower has to be applied to produce additional jets, in particular, low \pT\ jets.
Practically we assume that ME takes care of high \pT\ jets and PS does low \pT\ jets because ME jets are well modelled in high \pT\ region.
To ensure this assumption, reconstructed high \pT\ jets must match to partons produced by ME.
In addition, jets in some phase spaces are produced by both ME and PS, which leads to double counting of events.
For this purpose, MLM prescription~\cite{Mangano:2001xp}, CKKW-matching procedure~\cite{Hoeche:2009rj, Catani:2001cc} etc. are applied and
events are discarded if they cannot satisfy their requirement.
To explain MLM prescription in \textsc{Alpgen}, let us consider the production of $W$+up to 2-jets with a ME generator and jets with $p_\mathrm{T}>20$~GeV are used for data-analysis. First, events are produced with $W$ with 0 ME-jet, $W$ with 1 ME-jet and $W$ with 2 ME-jets, separately, where additional partons with, for example, $p_\mathrm{T}>15$~GeV are produced by a ME generator. Note that the parton shower is also applied, so that some high \pT\ jets might be produced by PS.
Then, all the reconstructed jets with $p_\mathrm{T}>15$~GeV are checked if they matched to ME-parton and we count such jets.
For $W$ with 0~(1) ME-jet, we require such jets should be exactly 0~(1).
For $W$ with 2 ME-jets, we require such jets should be 2 or more.
Then, we merge remaining events to make a $W$+up to 2-jets events.

\textsc{MadGraph}, \textsc{MC@NLO}, \textsc{Powheg} and \textsc{Sherpa} are used as NLO generators.
Additional one parton and also loop diagrams up to the next-to-leading order are properly taken into account.

\textsc{Photos} generates QED radiative corrections for charged leptons and quarks.
\textsc{Tauola} is a program to simulate tau-decay including polarisation properly.
They are optionally used in \textsc{Pythia} and \textsc{Herwig}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\subsubsection{Parton Distribution Function}

\subsubsection{Cross Section}
MC events are produced by using event generators, which can provide their cross sections including branching fractions.
However, in many cases, we don't use cross sections provided by event generators but
values obtained from dedicated programs,% to calculate cross sections,
because such programs can perform more higher order calculations than event generators.
NLO event generators are available for most physics processes on the marker but there is NNLO event generators.
On the other hand, the dedicated programs can calculate cross sections up to NNLO (or higher for some processes).
%cross sections can be calculated up to NNLO for them.

There is an important parameter, {i.e.} a renormalisation scale $\mu_R$, which is the scale at which the strong coupling is evaluated in order to calculate cross sections.
The value of cross sections does not depend on the choice of
$\mu_R$, however, since we cannot perform complete calculations
including all the orders of the strong coupling, the calculated cross sections might depend on $\mu_R$.
In the high energy region~($\gg O(100~\mathrm{MeV})$), a perturbative method works well in QCD like QED and we can calculate cross sections, for instance, up to next-to-next-to-next-to-leading order~(NNNLO, N3LO or N$^3$LO) for the Higgs gluon-gluon fusion production.

Including the higher order calculations, cross sections can be properly predicted and they are well consistent with the experimental results.
Table~\ref{table:xsec_ATLAS_summary} shows the results of $W$, $Z$, $t\bar{t}$, Higgs, $ZZ$, and $t\bar{t}W$ production cross section measurements as concrete examples with theoretical predictions.

\begin{table}
\begin{center}
\caption{Cross section measurements at the ATLAS experiment~($\sqrt{s}=13$~TeV) with theoretical predictions.
The measurements are given with statistical and systematic uncertainties.
The predictions are given with combined uncertainties including PDF, $\alphas$, scales etc.
For the Higgs production cross section, five processes are included: gluon fusion~(NNNLO), VBF~(approximate NNLO), $VH$~(NNLO/NLO), $t\bar{t}H+tH$~(NLO), and $b\bar{b}H$~(NNLO/NLO).}
\label{table:xsec_ATLAS_summary}
\begin{tabular}{l|l|l|c}
Process & Measurement & Prediction (higher order for QCD) & Reference \\ \hline
$W\rightarrow \ell\nu$ ($\ell = e, \mu$) & $20.64 \pm 0.02 \pm 0.70$~nb & $20.08^{+0.65}_{-0.66}$~nb (NNLO) & \cite{ATLAS:2016fij} \\
$Z\rightarrow \ell\ell$ ($\ell = e, \mu$)  & $1969 \pm 1 \pm 56$~pb & $1886^{+51}_{-57}$~pb (NNLO) & \cite{ATLAS:2016oxs} \\
$t\bar{t}$ & $826.4 \pm 3.6 \pm 19.6$~pb & $832^{+40}_{-45}$~pb (NNLO+NNLL) & \cite{ATLAS:2019hau} \\
Higgs & $55.4 \pm 3.1 {}^{+3.0}_{-2.8}$~pb & $55.6 \pm 2.5$~pb (NNNLO etc.) & \cite{ATL-CONF-2019-032} \\
%$WW$ & $130.04 \pm 1.7 \pm 10.6$~pb & $128.4^{+3.2}_{-2.9}$~pb (NNLO) & \cite{ATLAS:2019rob} \\
$ZZ$ & $17.3 \pm 0.6 \pm 0.8$~pb & $16.9^{+0.6}_{-0.5}$~pb (NNLO) & \cite{ATLAS:2017bcd} \\
$t\bar{t}W$ & $0.87 \pm 0.13 \pm 0.14$~pb & $0.60 \pm 0.07$~pb (NLO) & \cite{ATLAS:2019fwo} \\
\end{tabular}
\end{center}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Detector Simulation}
\label{mc_sec:detector-simulation}
Particles produced in the event generators are detected through the interactions with several detector components~(materials).
To reach the detector volume including the beam pipe, particles has long enough lifetime, that is, they are ``stable particles'' from a viewpoint of the detector simulation.
Such stable particles are electrons~($e^\pm$), photons, $\pi^\pm$, kaons~($K^\pm$, $K^0_S$, $K^0_L$), $\mu^\pm$, protons~($p$/$\bar{p}$), neutrons, neutrinos in case of the SM. In addition, in the SUSY and other new physics models beyond the SM, some particles, for example, the lightest neutralino is stable in the SUSY models with $R$-parity conservation. Some of stable particles, for example, $\pi^\pm$, $K^\pm$, $K^0_S$, $\mu^\pm$ can decay according to their lifetime in the detector volume, which is done in the detector simulation step, not by the event generator.

\textsc{Geant4} program~\cite{Agostinelli:2002hh} is a detector simulation toolkit and widely used in the experimental particle physics.
We build each detector component and define its interactions based on its real detector
in order to emulate how particles interact and how much energy of particles are lost;
energy loss and multiple scattering~(charged tracks in tracking volumes), electromagnetic interaction/shower, hadronic interaction/shower etc.
This is based on the best knowledge of the particle interaction with materials.
\textsc{Geant4} traces particles step-by-step and simulates their interaction.
This is a reason why MC simulation with \textsc{Geant4} is called {\it full} simulation.
%This is a reason why we use a term of {\it full} for MC simulations with \textsc{Geant4}.

%As we said in Section~\ref{mc_sec:overview}, there are ``fast simulation'' and ``parametric simulation''.
There are different types of MC simulations: ``fast simulation'' and ``parametric simulation''\footnote{The terminology to specify the type of MC simulation is not unique but depends on experiments. The machine learning techniques are also used for the MC simulation, for example, GAN.}.
In the parametric simulation, the detector response is described by expected resolution functions for each stable particle.
Momentum and energy are smeared with the resolution functions.
The effects of reconstruction and identification programs, that is, their efficiencies are replaced with weights or MC methods following their expected performance.
``Fast simulation'' is sometimes the same as the parametric simulation but this term is also used in case that a part of the detector simulation step, for example, calorimeter response, is replaced with a faster algorithm to emulate detector response.
In terms of the modelling of the real-data, in general, the full simulation is better than the fast and parametric simulations.
However, from the point of view of execution time, the full simulation is much slower;
for example, in some extreme cases, several minutes per event with the full simulation but less than a few seconds with the parametric simulation.
If we need lots of events to reduce the MC statistical uncertainties, the use of the fast or parametric simulations is one of the options.
In addition, it takes much longer time to develop computing programs with \textsc{Geant4}.

%\input{referenc_mc}
