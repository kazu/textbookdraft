%!TEX root = ../book.tex
%%%%%%%%%%%%%%%%%%%%% chapter.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sample chapter
%
% Use this file as a template for your own input.
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Jet Identification}
\label{sec:jetID}

\subsection{Fragmentation: Partons to Particles}
\label{sec:fragmentation}

This section describes the identification and reconstruction of
jets. A jet in high-energy physics is, naively speaking, 
a bunch of hadrons which are emitted
in nearby directions. This is an object consisting of the consequence 
of parton(s) fragmented into multi-hadron states. Here gives a short
introduction of how we understand the ``fragmentation" process, i.e.
the underlying physics of the partons transformed into long-lived
hadrons, followed by discussion on
algorithms to identify and reconstruct jets.

The partons, i.e. quarks and gluons, obey the dynamics described
by QCD with one and only parameter, the strong coupling constant  
$\alphas$. The coupling constant becomes smaller with the energy
of the interaction as a consequence of the renormalisation group
equation as shown in Fig.\,\ref{fig:alphas_running}. 
The energy scale, denoted as $\mu$, 
is given as the centre-of-mass energy of the partons
in concern. Since the energies involved in each parton
reaction is not measurable, the choice of the energy scale for 
a process is, however, %is 
not uniquely given
and we leave the discussion to elsewhere. Here we merely point out that
there are many choices: it could be centre-of-mass energy 
or transverse momentum of two partons when discussing 
on the parton-parton collisions,
often quadratically summed with a heavy quark mass if a heavy
quark is involved, or the mass of the particles ($W, Z, \Upsilon$ ...)
when discussing the decay of particles.

Now let us take a simple example, a decay
of the $Z^0$ boson into a $q\bar{q}$ pair for understanding
how a parton fragments into a multi-hadron state. The energy scale would
be given as $\mu = m_{Z^0}$. In this case the quarks cannot be a pair of
top quarks due to energy conservation and the quarks run fast and
may radiate additional gluons, since a quark feel the force
from the other quarks due to the colour charge carried by each
of quarks. The force is ``strong", as $\alphas$ is about 0.1
at the mass scale of $m_{Z^0}$. The radiation of the gluon is
{\it soft} in most cases, i.e.
typically collinear and/or with small momentum
fraction with respect to the parent quark, like 
for the case of bremsstrahlung.
But with a small probability the gluon may have large angle from 
both of the two quarks {\it and} may have large momentum fraction,
i.e. the radiated gluon may be {\it hard}. 


The gluons and quarks still feel the colour force and may further radiate 
a pair of $q\bar{q}$, $g \rightarrow q\bar{q}$ or radiate further a gluon
$g \rightarrow gg$. The splitting of partons would be repeated:
this process is often called ``parton shower".
% which is also partially perturbatively calculable by so-called
% leading logarithmic resummation technique.
After some steps of radiation,
the partons are branched into many parton states, most of which
have another close-by parton and the invariant masses between these
two partons are much smaller than the
initial mass $m_{Z^0}$. In such a situation, the coupling constant 
describing the interaction of the partons becomes much
larger, say 0.3 rather than 0.1. This accelerates the process
of the fragmentation and eventually all the partons would have
their invariant mass with the nearest partons below 1\,GeV.
This is the energy scale of $\Lambda_{QCD}$, below which perturbative
QCD (pQCD) is no longer applicable; one cannot discuss the branching
of partons by perturbation theory and need a help of the non-perturbative
approach, e.g. the lattice QCD. 

The lattice QCD calculation tells us that the potential energy
of a $q\bar{q}$ pair is linear to the distance $r$ between the pair, 
$U(r) \propto r$. This means that the line of the strong force is
about constant density and concentrated in a tube-like area. The stored
energy gets higher as the distance becomes larger. Once the stored energy
exceeds beyond the mass of two quarks, the total energy should be
lower if a $q\bar{q}$ pair is produced and the force lines are cut. This
process continues until the relative distances between all 
the colour-neutral $q\bar{q}$ pair becomes shorter that there remains
no more enough energy to produce additional $q\bar{q}$ pair, giving
the end of the showering process. All the quarks and gluons
are in bound states, i.e. mesons or baryons, at this stage. This last
part of the transition is called hadronisation.

Prior to the discussion on jet algorithm, we may like
to see if the input to the algorithm, the four-momentum of
the final state objects, are well defined.
The final state with hadrons can clearly be defined once we give
a threshold on the life time of the final state particles. The boundary
is often given at where the $B-$mesons and charm mesons
decay but not charged pions. The intermediate states of partons before
hadronisation, on the other
hand, are less obvious in their definition and we need
certain criteria, which we discuss in the following section.
One should
also note that the fragmentation is a process described by quantum
field theory and it is not possible to assign a certain parton or hadron
to their parents in principle -- what we know through the theory
is the probability to which parents the daughter particle
is assigned, unless the lifetime of the parent particle is long
enough that the quantum effect is negligible. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Defining Jets}

While it is impossible to have one-to-many correspondence between
a parton and hadrons, one may
still imagine that a spray of particles, or a jet, would be originated 
from a quark or a gluon,
if it looks like collimated and away from the other activities,
and may like to relate the jet to the underlying parton.
This relies on the fact that the parton emission is mostly collinear
if the parents of the hadronised partons have sufficiently
high energy and run fast.
In practice, however,
the jets are still ``ambiguous": if there are two close-by jets or a 
wide jet, it is often not straightforward to know whether to associate
a parton or two or more partons to the jets. A hadron away from the sprays
may also be ambiguous in such an assignment or left unassociated to any jets.

Things are even more complicated if we are to reconstruct jets
using the detector information.  
Not only the momentum of the particles are smeared by the detector, 
but also that a significant part of the particles may be escaped
from detection. Also a measurement by calorimetry cannot
resolve two close-by hadrons since their energy clusters may be
merged to a cluster if the distance between the two hadrons
at the calorimeter is less than a certain value.

% This means that the variables
% used in reconstructing the momentum of the jet should be {\it linear} 
% function of the momentum i.e. the first order in momenta, so that it
% is independent of if the particle was measured as one, many or a part
% of object(s).

This also applies when we extend the concept of the jets to
partons. Parton branching is also a quantum-mechanics
process and the final state partons are not uniquely related
to their parent partons, 
as described above.

%In the lowest order diagrams of parton-parton scattering,
%there are two back-to-back high-$\pt$ partons in the final state; the
%two partons are well separated. 
%%%% we have a sub section earlier to describle the things below
%%%% more in detail.
%In the dynamics of partons, however,
%the higher order diagrams are very important. A parton may emit another
%parton in a high probability when the primary parton has high
%momentum (the scale of the interaction $\mu \gg 1$\,GeV) and 
%the strong coupling constant $\alphas(\mu^2)$ is order of 0.1. 
%After a few steps of such parton branching the partons loose
%momentum and $\alphas$
%would become large when $\mu$ is close to order of 1\,GeV, the parton
%branching occurs very often. 
%%%% end of the things to be written in other section
%This all means that the number
%of partons in the final state is not well defined either. It does not
%make much sense to classify the events using the number of partons. Yet,
%the high-energy partonic final states do, in many of the cases, 
%have some distinct number of splush if you see the event displays
%(see ...).

These facts all call for some definition of jets, or a jet algorithm that
defines the number of jets and their momenta.
% the problem is apparently
% quite similar to pattern recognition.
The algorithm has
to be independent of the number of final states and also the type
of particles: a few ``primary" partons, many partons after the parton
shower, hadrons before or after the meson decays, or 
detector measurements. The algorithm should also identify, count
and reconstruct the momentum of hard, i.e. high momentum partons 
while the soft emissions nearby hard partons should be absorbed
to the hard partons, or discarded. In this sense the algorithm
should be insensitive to the soft emissions, or ``infrared safe".
% like as discussed in thoeoretical model in particle physics.
In fact, the procedure to absorb the soft
particles to the stronger jets is somewhat analogical
to the procedure of renormalisation in theoretical calculation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Jet Algorithms}
\label{sec:JetAlgorithm}

Historically there have been two kinds of jet algorithms used
in high-energy physics, one called the cone algorithm and the other
the cluster algorithm. The cone algorithm moves around a window of
jet area defined as a circle in $\eta-\phi$ space, 
$\Delta r = \sqrt{\Delta \eta^2 + \Delta \phi^2} < R$, where
the $\Delta$ variables $\Delta r$, $\Delta \eta$ and $\Delta \phi$ 
are the distance between the jet centre and the position of
the particle. $R$ is called cone radius, giving the angular
boundary of jets.
The algorithm iteratively find such an energetic cluster where
the sum of the transverse momenta $p_{\rm T}^{\rm jet}$ 
for the particle inside the circle
becomes maximum. The cluster with transverse momenta is said to be a jet
if $p_{\rm T}^{\rm jet} > p_{\rm T}^{\rm thr}$, the threshold value of the jets,
which is the parameter to ensure that the jets are hard. After the algorithm
is run, one may find many jets but also many particle clusters 
below the $p_{\rm T}^{\rm thr}$, which are not qualified to be a jet
and discarded. This feature is suitable
for hadron-hadron collisions where hard jets are 
accompanied
with many soft particles arising from soft emissions from beam remnants,
particles from soft underlying events (rescattering
of the outgoing proton remnants) and
multi-parton interactions, those often called ``underlying events".
In addition, particles form soft collisions pile up to the hard
partons in case of high-luminosity collisions such as the main LHC runs
(see Section~\ref{sec:calib_jet}).

There still remains the activity of particles not emerged
from the hard partons within the cone. Although the amount
of the underlying events and pile-up particles are certainly not constant,
it is at least possible to
statistically subtract such contributions since the size of the jet
is the same for each jet unless they overlap and some part
of the jet area is to be shared; even for such a case the net area 
of overlapped jets is still well defined. 
This is another virtue of the cone algorithm.


The historical cluster algorithm, on the other hand, assigns all the particles
to one of the jets. This is suitable for $e^+e^-$ collisions where there
are neither beam remnants, multi-parton interactions nor pile-up but only
the soft emissions from hard partons. 
The basic idea is that the soft particles should always
be merged to other particle or nearest cluster until the energy of the cluster
exceeds beyond the threshold (see Fig.~\ref{fig:jetclus}). The ``distance" between two particles, $d_{ij}$
is defined in various ways, which gives the variation and choice
to the algorithm. The distance can be an invariant mass squared between
two particles (the original JADE algorithm) or relative transverse momentum squared
of a softer particle with respect to the harder particle (often 
called $k_{\rm T}$ or $k_\perp$): $d_{ij} = min(p_{\rm T}^{i2}, p_{\rm T}^{j2})$. 
The algorithm sequentially
combines two particles of the nearest distance. In each step of
the combination, the four-momentum of the two merged particles is
calculated to form a new particle. There are also many choices in
how to combine the momenta of two particles 
(called ``recombination scheme") -- either the merged 
particle is massless, massive, conserving energy or not etc. 
The definition of distance and recombination scheme should be chosen such that
the jet observables in concern (momentum, energy, number, mass etc.) are
well reproduced and the choice may vary with energy and type of the interaction.
The combination is stopped until the distance
between two jets, defined as $y = d_{ij} / M$ become above $y_{cut}$,
where $M$ is normally chosen as the invariant mass of the first two
outgoing partons from the $e^+e^-$ collisions, which equals to
the centre-of-mass energy of the $e^+e^-$ collisions in most of the cases,
except for the events with hard initial state radiation of photons.

\begin{figure}
\begin{center}
\includegraphics[width=11cm]{y_figs/fig_6_4_ktalgo_merge.png}
\caption{A schematic drawing, showing how clusters with nearest distance
are merged each other to form a new cluster.}
\label{fig:jetclus}
\end{center}
\end{figure}


The biggest advantage of the cluster algorithm against the cone
algorithm is that there is no ambiguity in the algorithm originated
from the iterative procedure in the cone algorithms.
The cone algorithm needs seeds to
start the iteration. It is well known that the number of jets and
jet momenta is largely affected by the choice of the property of the seed
($\pt$ threshold and the cone size to define a seed) when particles
are densely populated. A jet could be split into two jets
depending on the seed choice. This means that the result of the 
jet finding is affected by soft particles (which could be the seed). It is known
that a naive cone algorithm is not infrared safe
i.e. the result of the algorithm may depend on 
a presence of a particle with infinitesimally small energy.

A new class of cluster algorithms
for hadron-hadron collisions are then invented, by
taking virtue of the cone algorithms, (a) the algorithm works on 
$\eta-\phi-\pt$ space so that it is boost invariant and (b) particles
below the threshold are discarded. A typical arrangement is to introduce
two particles with infinite momentum on the beam axis. In the algorithm,
the distance
between the beam particles $d_i$ is defined as $d_i = p_{\rm T}^{i2}$, where
$p_{\rm T}^i$ is the transverse momentum of the $i$-th particle,
in addition to $k_{\rm T}$ as the distance parameter between two
final state particles, $d_{ij}$.
If $d_i$ of the particle, the distance to the beam axis,
is smaller than any of $d_{ij}$'s, the distances
to the other final state particles,
the particle is merged to the beam particle.

Also $d_{ij}$ is adjusted to the hadron collider environment.
The first version of the algorithm, the $k_{\rm T}$ algorithm, 
uses the distance parameter as

\begin{equation}
  d_{ij} = \min(p_{\rm T}^{i2}, p_{\rm T}^{j2}) \Delta r_{ij}^2 / R^2
\label{eq:dijkt}
\end{equation}
where $\Delta r$ is that used in the cone algorithms,
$\Delta r = \sqrt{\Delta \eta^2 + \Delta \phi^2}$, and $R$ is the radius
parameter. The particle
with the smallest $\pt$ will be merged to the beam if $\Delta r$ is
more than $R$ for any other particles,
since then $d_i$ would be smaller than any of $d_{ij}$. 
It will be merged to the nearest particle if $\Delta r < R$.
In this way the parameter $R$ plays the role of the cone radius
in cone algorithms.

\begin{figure}
\begin{center}
\includegraphics[width=10cm]{y_figs/fig_6_4_ktalgo.png}
\caption{A schematic drawing, showing how clusters with large momenta are classified: to be merged each other to form a new cluster, or to the beam axis to be considered as a jet.}
\label{fig:boost_kt}
\end{center}
\end{figure}

 
The value of $R$ gives the angular size of the jets. This
is a parameter to which extent one allows to include hard parton
radiation around the primary parton, in addition to the soft emissions
and/or collinear part of radiated partons. The size should not
be too small to include the soft/collinear particles,
but should not be too large
since the particles from beam-related activities (soft underlying
events, multi-parton interactions and pile-up particles) may come
more into the jet area. Typical values used for the QCD studies
at the energy scale of weak interactions ($p_{\rm T}^{\rm jet} \simeq m_Z / 2$)
is $0.6-0.7$ to include soft emission originated from the parent
partons, in order to reduce the theoretical uncertainty in pQCD description
of the data. For higher energy interactions,
$0.4-0.5$ would be more preferred,
in particular for physics beyond the SM~(BSM) searches at TeV scale,
to minimise the effect of soft particles to the momentum or mass
reconstruction of the parent BSM particles.

The clustering procedure is finished when there is no possibility
to merge the remaining particles each other except for the beam
particle. The particles above a given $\pt$ threshold is defined as
jets and others are discarded, i.e. merged to the beam particle.

The original $k_{\rm T}$ algorithm, where $d_{ij}$ is 
defined as Eq.~(\ref{eq:dijkt}), it is known that the jet area tends
to be extended beyond the area given by the parameter $R$. This feature
is undesirable for the mass reconstruction as discussed just above.
Recently {\it anti-}$k_{\rm T}$ algorithm became more popular,
where $d_{ij}$ is defined as:

\[
  d_{ij} = \min((p_{\rm T}^{i})^{-2}, (p_{\rm T}^{j})^{-2}) \Delta r_{ij}^2 / R^2
\]
With this distance parameter, the algorithm first merges the pairs
within the maximum allowed distance, $\Delta r_{ij} \simeq R$, making a merged
particle in between. The same thing happens in the next iteration: the furthest
particle from the new merged particle would be absorbed. This would
imply that the direction of the jet particle, or the jet axis, would be
oscillated between the merged particles, but the axis will be stabilised
in the later stage where only particles with small $k_{\rm T}$ with respect
to the jet axis are left, which are eventually be merged to the jet.
As a consequence, the jet area will have clear boundary of a circle
with the radius $R$. The area of overlapping circles will be absorbed
to more energetic jets.
This would give the jets very similar to what is given
by the cone algorithm, which has certain area size. One can
statistically subtract the underlying events of the jets in such a case.
% 
The anti-$k_{\rm T}$ algorithm combines virtues of the cone and
cluster algorithm successfully and is now the most
popular jet algorithm at the LHC.

%%% here OK?
Naturally the criteria to define the jets
of the partons is based on continuous parameters, such as  $\pt$, $R$,
which have no characteristic scale, apart from $\Lambda_{QCD}$,
being anyhow much below the typical jet momentum ($> O(10)$\,GeV). 
There are some arbitrariness on 
the parameter values in such algorithms and the parameters may
have to be optimised for each application.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Calibrating Jet Measurements}
\label{sec:calib_jet}

As described in Section~\ref{sec:fragmentation}, the jets
are expected to be collimated at high energies, primarily since radiation in
the final state is less pronounced for high-$\pt$ jets where
the coupling constant $\alphas$ is smaller. There the individual
hadrons consisting of a jet cannot easily be resolved and
the calibration of the detector is performed at the level
of jets instead of constituting hadrons. The result of jet calibration
is often called jet energy scale~(JES).
 
Since a jet is defined by means of an algorithm, the momenta of 
jets depend on the choice of the algorithm and jet finder parameters
(e.g. $R$).
The calibration on jets should be repeated for each choice of the algorithm
and the set of parameters. There is another point to choose:
if the energy is to be corrected to the ``particle level", i.e.
jets using the momentum of particles, or to the parton level, where
partons are the input for the jet algorithm.
A general consensus is that the correction to the particle
level is to be applied, i.e. the momentum of the particle-level
jet gives the reference to a detector-level jet, which matches
in $\eta-\phi$ space. In this way one can avoid the theoretical
uncertainty on the correction factor from the particle to
the parton level jets, which is expected to be improved
as the theory is more advanced.

A simplest reconstruction of jets is to start from
only calorimeter information, as described in
Section~\ref{sec:JetAlgorithm}. The energy calibration for calorimeter
objects are done to the electromagnetic scale, ignoring that
$e/h \neq 1$~(see Section~\ref{sec:calib_hadron}),
or to the hadronic scale after applying $e/h$ correction.
% Here $e/h$
% is the ratio of the response of the calorimeter for electromagnetic
% particles ($e$ or $\gamma$) to hadrons.
In principle a calorimeter cluster can be replaced with a matched
track to improve the momentum resolution, if the spacial resolution
of the calorimeter cluster is fine enough to resolve close-by particles.

There remains still difference between the particle-level and detector-level jets.
In addition to the factors arising from calorimetry, such as longitudinal
hadron shower leakage and intrinsic dependence of calorimeter
response on type of particles (the $e/h$ ratio, response for muons
and neutrinos), the following effects specific to jets cause significant shift in
measured momentum:

\begin{itemize}

\item particles escaping outside the jet area
\begin{quotation}
If the jet area is wider than the radius used in the jet finder,
the particles outside the jet is lost and the jet energy
is underestimated. As explained above, the size of the jet
is wider for low energy jets since the partons are radiated
more often. This leak, however, should be a part of the jet
definition for both parton and particle-level jets. Further
leak occurs when the particles are bent by solenoidal magnetic field
applied to the central tracker. This is to be corrected
through the jet energy scale.

A gluon radiates another partons more often than a quark because
of the different colour factor (9/4 vs. 1).
In general a gluon jet is wider than a quark jet and particle
spectrum is softer because of more radiation. 
A gluon jet contains 
more particles with smaller average energy than for a quark jet.
This again leads to more leaks by the magnetic bending of particles.

An additional correction
depending on the jet properties, such as the transverse size
of the jet or the number of tracks matched to the jet, would improve
the energy resolution of jets.
\end{quotation}

\item response of heavy-quark jets
\begin{quotation}
Some shift may remain for $c$-quark and $b$-quark jets ($c/b$-jets). 
A $b$-jet may decay semi-leptonically, 
to a lepton ($e, \mu, \tau$), a neutrino and 
a lighter $c$- or $u$-quark jet. The $c$-quark jet
may decay again semi-leptonically. As a consequence, $c/b$-jets may
contain one or more electron or muon and one or more neutrinos.
The momentum of neutrinos cannot be measured; moreover,
the muon leaves only up to two GeV energies in calorimeter~(MIP). Therefore,
the energy responses for $b$- and $c$-quark jets are in general
smaller than other kinds jets ($u, d, s$ or gluon jets --
often called ``light flavour jets"). The actual difference
depends, then, on many factors e.g. on how the muon momentum is taken
into account, the $e/h$ ratio, which may affect the jet energy
containing an electron etc. Anyhow, it is a common practice
to apply additional correction if a jet is identified as a heavy
quark jet.
\end{quotation}

\item pile-up 
\begin{quotation}
%YY For $pp$ or $p\bar{p}$ colliders with 
% high luminosity $(> 10^{32} {\rm cm^{-2}s^{-1}})$, 
% a crossing of a pair of bunches would cause more than one interaction
% of a proton-proton collisions because of high total cross section
% of $pp$ interaction. At the LHC, typical number
%YY of interactions is 20 -- 40 for the nominal luminosity,
% $> 10^{34} {\rm cm^{-2}s^{-1}}$.
% which corresponds to the number
% of pile-up interactions to the event of interest.
One can safely assume
that the events that piles up on top of a collision of interest are all
soft interactions (see Section~\ref{sec:natureHHcoll}). The soft
interaction events are often called "minimum-bias" events since
the events are taken through triggers as little requirement as possible,
e.g. small energy in very forward part of the calorimeter.
Average $\pt$ from such minimum-bias events at the LHC energy
($\sqrt{s} \simeq 14$\,TeV) is about $2.4$\,GeV per unit
of $\eta-\phi$ space. This means that a jet with radius $R = 0.4$ 
with 20 additional minimum-bias events have average offset of $\pt$
% Why is this not 1.2 GeV? (Hanagaki)
by about $24$ GeV, hence gives a very large shift in energies
for jets with $p_{\rm T{\rm jets}} < O(100)$\,GeV.
In order to reduce the influence from pile-up particles, the expected
average $\pt$ from pile-up is subtracted. The actual value to be
subtracted depends on the number of pile-up events. The average
number of pile-up can be estimated from the luminosity of the collisions.
The Poisson fluctuation from the average can further be corrected
by measuring the number of interactions per bunch crossing through,
e.g. $N_{\rm PV}$, the number of primary vertex per crossing
reconstructed from the central tracker.
\end{quotation}

\end{itemize}

The residual difference from imperfect simulation of 
the detector and remaining
miscalibration of detectors is corrected by in-situ 
measurements of jet response.
The most common way to determine the overall jet energy
scale is to find and use some physics processes with a jet
whose energy can be deduced through energy-momentum conservation.
In the hadron collider experiments, for example,
the production of $\gamma +$jet or $Z +$jet is
widely used as the calibration source, where the photon or $Z$
reconstructed from dilepton can be the reference to the jet energy
because the fluctuation of energy deposited by the
electromagnetic shower or measured charged track momentum 
is much smaller than that by the hadronic
shower, leading to more precise energy measurement than
that by the hadron calorimeter.
However, since the momentum conservation is hold only in the plane
perpendicular to beam axis in the hadron collider,
what is conserved is $\pt$, not $p$.
More concretely, in $\gamma +$jet event, the jet energy scale is adjusted
so that the $\pt$ of the jet is equal to that of the photon.
The result of the calibration is illustrated in
Fig.~\ref{fig:calib_gam-jet}.
The clear peak can be seen in the $\gamma +$jet events
with the peak close to unity as expected.
% The same idea applies for the $Z + jet$ where leptonic 
% decays of $Z$ can be reconstructed with much better momentum
% resolution than that of jet.




\begin{figure}
\begin{center}
\includegraphics[width=8cm]{calibration/fig_gam-jet.pdf}
\caption{
The ratio of $\pt$ of jet to $\gamma$ for the $\pt$ range
between 160 and 210~GeV~\cite{ATLAS:2014hvo}~(ATLAS Collaboration {\copyright} 2014 CERN).
The calorimeter region is restricted to be $|\eta|<1.2$.
}
\label{fig:calib_gam-jet}
\end{center}
\end{figure}
%\begin{figure}
%\begin{center}
%\includegraphics[width=5cm]{calibration/fig_z-jet.pdf}
%\caption{
%The ratio of $\pt$ of jet to $Z$ for the $\pt$ range
%between 20 and 25~GeV.
%The dots (histogram) represent the data (simulation)
%\cite{ref:jetenergyscaleplot}.
%}
%\label{fig:calib_z-jet}
%\end{center}
%\end{figure}

%On the other hand, the peak position by the $Z+jet$ is shifted
%to below unity because the $\pt$ values are small calibrated
%in this method is relatively small and the energy loss of particles
%affects more directly to the shift in $\pt$ fraction. 
%% (YY: I don't quite understand here: does it mean pt is small and
%% response is far from unity?) - tried to fix
%Note that the hadronic response strongly depends on $\pt$ or
%energy.

With the similar concept of calibrating the electromagnetic scale,
$Z \rightarrow q \bar{q}$ can
be used in principle as the calibration source of the jets with
the $Z$ mass as the reference target.
However, this method does not work in the hadron collider
experiments in practice, because of the overwhelming dijet
backgrounds generated by QCD process.
In addition, the jet energy resolution is much worse than that of
the electromagnetic energy measurement, resulting in the
difficulty to see the resonant peak from $Z\rightarrow q \bar{q}$.

%The in-situ calibration parameters are obtained from
%residual difference between the simulation and the actual data.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Reconstructing Missing Momentum}

The momentum of the neutral particles,
such as neutrino and unknown neutral particles, are not detected
by collider detectors. For hadron colliders, the longitudinal
momentum of such particles cannot be known due to the lack of
longitudinal momentum information of the collisions.
The missing transverse momentum,
denoted as either $p_{\rm T{\rm miss}}$ or $\vec{E}_{\rm T{\rm miss}}$, can
still be reconstructed by the negative of the transverse momentum
vector of observed particles. A first approximation
of the sum of the visible particle momentum could
simply be obtained from the $x$ and $y$ component of the calorimeter
cell energies, $E_{i,{\rm cell}}\sin\theta_i\cos\phi_i$
and $E_{i,\rm cell}\sin\theta_i\sin\phi_i$. This would miss muon
momenta and also detailed calibration depending on the final state
objects are ignored. Instead, one may measure each category
of final state objects separately, with proper calibration and possibly
with a help of tracking and muon detectors, for example,
\[
\vec{p}_{\rm T{\rm miss}} = \sum \left[ \vec{p}_{\rm T}^e + \vec{p}_{\rm T}^{\mu} + \vec{p}_{\rm T}^\gamma
  + \vec{p}_{\rm T}^\tau + \vec{p}_{\rm T}^{\rm jets} + \vec{p}_{\rm T}^{\rm others} \right] ,
\]
as is done for the ATLAS experiment. 
Here $p_{\rm T}^{\rm others}$ term is the momentum of the particles
belonging to neither of objects identified as charged lepton, jet nor a photon. 
This term, often called as ``soft term", includes the rest
of the particles accompanied with the hard interaction,
such as particles from ISR,
multi-parton events and underlying events, which should also be added
to the $p_{\rm T{\rm miss}}$ calculation. The soft term, however, also
includes particles from pile up. Since average transverse energy
of a minimum-bias event is about 100 GeV, the total transverse energy
of an event with $>20$ pile-ups would be about 2 TeV and increases
proportionally as a function of the number of pile-up. The resolution
of this pile-up component directly affects the missing
$\pt$ calculation. Therefore, the performance of the missing $\pt$
reconstruction strongly depends on how to estimate the missing vector
from the soft term, and to less extent through the jet term.
In addition, any misreconstruction in the detector,
such as noise in the calorimeter, affects to $\vec{p}_{\rm T{\rm miss}}$
through the soft term.

Various algorithms are developed
in order to mitigate the growth of the resolution with the number
of pile-up events. A simple algorithm is to reconstruct the soft term
by using only the calorimeters or the central tracker. The latter
has a benefit that it can remove all the track momentum not originated
from the vertex of the hard scattering in concern. It misses, however,
the contribution from the neutral particles like $\pi^0$ and $K^0_L$.
A further refined algorithm could be to reweight the calorimeter soft term by
the momentum fraction of the soft term from trackers from the primary
vertex (PV): $\Sigma_{\rm tracks,PV} \pt / \Sigma_{\rm tracks}$. 
Each estimator would have different resolution and tail.
Now suppose that we find long tail in for zero missing-$\pt$ events
on truth level.
% events with zero missing momentum may give a large tail for
% either calorimeter soft term or tracking soft term.
It is found there that tails of the soft term of tracking and calorimeter
algorithms are not strongly correlated.
For that reason it is often useful to use more than
one algorithm to reduce the tail induced by the $p_{\rm T{\rm miss}}$
reconstruction. This also indicates that the choice of
the soft term reconstruction depends on the type of events
in concern. 
