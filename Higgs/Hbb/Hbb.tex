\subsection{$H\rightarrow b\bar{b}$}

This section outlines the analysis of $H \rightarrow b\bar{b}$.
As the branching fraction of $H \rightarrow b\bar{b}$ is the largest ($\sim 58\%$) among the
various decays of Higgs of 125~GeV mass, $H \rightarrow b\bar{b}$ could be the most useful and natural
decay mode to search for Higgs and to study its properties in view of the
statistics.
On the other hand, the signature of the final state consists of just two $b$-jets.
There would be no issues in case of the $e^{+}e^{-}$ colliders such as ILC, which
provide very clean environment experimentally, resulting in very high signal-to-noise ratio.
In the hadron colliders, however, the study of $H \rightarrow b\bar{b}$
is not straightforward at all because of the overwhelming QCD backgrounds (multijets background processes).
At the energy of LHC, for example, the production cross section of inclusive
$b$-jets is larger than by 8-th order of magnitude than that of the Higgs.
In addition, the identification of $b$-jets is not perfect.
Light jets can mimic the signal.
In this case, any jet production can be a background, whose production cross
section is even higher than the inclusive
$b$-jet cross section.
Therefore at the hadron colliders we need some clever idea to separate
the $H \rightarrow b\bar{b}$ signals from the huge background.

In the following, we discuss the analysis method of $H \rightarrow b\bar{b}$
using the vector boson fusion process first and then the associate production
of $W$ and $Z$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Vector Boson Fusion Process}

The final state consists of two $b$-jets decayed 
from Higgs and two forward jets.
Since there are no isolated leptons or large missing $E_{\rm T}$ which are commonly
used to trigger an event, the careful study and the optimisation of the trigger is needed.
The most apparent choice of the trigger would be to require four jets with
relatively high $\pt$.
In addition a requirement on the topology, i.e. the existence of two forward jets
in different $\eta$, respectively, may be applied if such a topological trigger 
is available.
Even with the requirements above, still the remaining events would be dominated
by the multijets background because of the huge production cross section.
In order to suppress the multijets events further, the existence of
a muon (see Section~\ref{sec:btag_softlep})
that arises from the semi-leptonic decay of $b$-hadrons (directly or through the
cascade decay to $c$ like $b\rightarrow c\ell^-\bar{\nu}$) may be required with a cost of statistics.
Even though there are two $b$-hadrons (and hence two $c$-hadrons followed by the
decay of $b$-hadrons most of the time), the branching fraction of semi-leptonic decay
is only the order of 10\% (see Section~\ref{sec:bjet}) .
The $\pt$ of the lepton from the semi-leptonic decay is not so large.
Because of these two factors, the signal efficiency is relatively low. %not relatively high.
Therefore one has to optimise the trigger condition with a careful study.
In other words, this is where the improvement potentially exists.

The offline analysis starts from selecting events with four jets.
Out of the four, two are required to be in the central (rather small $|\eta|$),
and the other two in the forward region ($\equiv$ forward jets).
The forward jets tend to keep the direction of the parents' protons, and hence
to be in the opposite region in $\eta$ % terms of $\eta$ with respect to zero.
%This can be seen in Fig.~\ref{fig:VBF} that shows $\eta$ of forward jets and the one
%from $t\bar{t}$ decays as a comparison, implying the the uniqueness of
%the topology of the VBF process.
In order to select only the VBF process, commonly used requirements for the
forward jets are to have large separation in $\eta$ between the two, where if
one in $\eta > 0$ then the other must be in $\eta<0$,
% or vice versa,
and to have
large invariant mass reconstructed from the two forward jets.
%\begin{figure}
%\begin{center}
%\includegraphics[width=7cm]{Hbb/fig_VBF_forwardjets.pdf}
%\caption{
%The $\eta$ distributions of forward jets in the simulated events.
%The black histogram shows the distribution of jets that emit either $W$ or $Z$
%bosons that are fused to generate Higgs boson.
%}
%\label{fig:VBF}
%\end{center}
%\end{figure}

Once an event pass the selection criteria for the forward jets, the remaining part
is rather straightforward.
The two central jets must be identified as $b$-jets, where there is always a
room of the optimisation or tuning of the $b$-tagging requirement.
For example, a requirement of at least one $b$-tag is also possible.
The tightness of $b$-tagging requirement is another knob for tuning.
Finally we look for a signal peak in dijet mass distribution,
which is reconstructed from the
two central jets.
%, distribution.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Associate Production with $W$ or $Z$}

The idea behind using the associate production with $W/Z$ is to exploit
an isolated lepton from $W/Z$ decay to reduce background.
In both trigger and offline event selection, an event is required to have at least
one isolated lepton with some criteria such as $\pt$ or $\eta$.
Then in the offline selection, $W$ can be identified by reconstructing transverse
mass from the isolated lepton and the missing $E_{\rm T}$.
In case of $Z$, dilepton mass is a powerful tool to separate the signal out from
backgrounds.

\begin{figure}
\begin{center}
%\includegraphics[width=7cm, angle=270]{Hbb/fig_Hbb.pdf}
\includegraphics[width=7cm]{Higgs/Hbb/fig_VHbb.pdf}
\caption{
The invariant mass distribution reconstructed from two jets~\cite{ATLAS:2020fcp}~(ATLAS Collaboration {\copyright} 2020 CERN).
The dots represent data.
The red histogram shows the expected signal contribution where
the signal yield is assumed to be 1.06 times the standard model expectation.
The grey histogram shows the expected background contribution by $ZZ$ or
$WZ$ events.
}
\label{fig:Hbb_VH}
\end{center}
\end{figure}

The procedure after selecting or tagging $W/Z$ is very similar to that in the
VBF analysis.
The dijet mass reconstructed from $b$-tagged jets is the most efficient
variables to discriminate signal from background.
In the end, the dominant source of backgrounds is $W/Z$ production
associated with heavy flavour jets, whose final state is exactly the same as signal.
On top of that, $t\bar{t}$ production is also a main component of the
remaining background.
Therefore, jet energy resolution to identify a possible peak from
$H \rightarrow b\bar{b}$ decay is one of the most important key elements
in this analysis, as well as the efficiency to detect and identify the final state objects.
Figure~\ref{fig:Hbb_VH} shows the distribution of dijet mass reconstructed from two
$b$-tagged jets in the ATLAS experiment, where all the expected background
contribution, except for $VZ, Z\rightarrow b\bar{b}$~$(V=Z$ or $W)$, is subtracted.
One can see a peak by $Z\rightarrow b\bar{b}$ as well as the small
enhancement around 125~GeV, which is the evidence of $H\rightarrow b\bar{b}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%\begin{thebibliography}{99.}
%
%\bibitem{ref:VHbb}
%G.~Aad {\it et al.} [ATLAS Collaboration],
%Eur.\ Phys.\ J.\ C {\bf 81}, 178 (2021).
%
%\end{thebibliography}
